#include "../common.h"

INIT_SECCOMP_FLAG();

int main(void)
{
    INIT_SIGNAL_HANDLER_EXPECT_ALLOWED();
    syscall(SYS_getgid);
    TEST_SECCOMP_EXPECT_ALLOWED("getgid", SYS_getgid);

    INIT_SIGNAL_HANDLER_EXPECT_BLOCKED();
    syscall(SYS_geteuid);
    TEST_SECCOMP_EXPECT_BLOCKED("geteuid", SYS_geteuid);

    INIT_SIGNAL_HANDLER_EXPECT_ALLOWED();
    syscall(SYS_getuid);
    TEST_SECCOMP_EXPECT_ALLOWED("getuid", SYS_getuid);
}
