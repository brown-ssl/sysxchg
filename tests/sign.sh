#!/bin/bash

IMA_GROUP="ima"
IMA_KEY="/etc/keys/privkey_ima.pem"
EVM_KEY="/etc/keys/privkey_evm.pem"

BINS="${1}"

if ! grep -q "^${IMA_GROUP}:" /etc/group; then
		addgroup --system "${IMA_GROUP}"
fi

for bin in ${BINS}; do
		# skip files that already signed
		if [[ -n "$(getfattr -m . -d "${bin}")" ]]; then 
				echo "[+] Binary already signed, skipping... (${bin})"
				continue
		fi

		# change file owner's group to be ima for easy policy appraisal
		chown :"${IMA_GROUP}" "${bin}"

		# sign for IMA first...
		evmctl ima_sign --key "${IMA_KEY}" "${bin}"
		# ...then EVM
		evmctl sign --key "${EVM_KEY}" "${bin}"

		echo "[+] Signed binary ${bin}"
done
