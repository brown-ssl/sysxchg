#include <unistd.h>
#include "../common.h"

INIT_SECCOMP_FLAG();

int main(void)
{
    TEST_DESCRIBE("Testing exec seccomp filter -> exec seccomp filter");
    INIT_SIGNAL_HANDLER_EXPECT_ALLOWED();
    syscall(SYS_getgid);
    TEST_SECCOMP_EXPECT_ALLOWED("getgid", SYS_getgid);

    INIT_SIGNAL_HANDLER_EXPECT_ALLOWED();
    syscall(SYS_geteuid);
    TEST_SECCOMP_EXPECT_ALLOWED("geteuid", SYS_geteuid);

    INIT_SIGNAL_HANDLER_EXPECT_BLOCKED();
    syscall(SYS_getuid);
    TEST_SECCOMP_EXPECT_BLOCKED("getuid", SYS_getuid);

    fflush(stdout);

    char *prog = "./inheritance/bin/8-exec-seccomp-2";
    int ret = execve(prog, (char *[]){prog, 0}, NULL);
    if (ret)
    {
        printf("execve returned %d\n", ret);
        printf("errno: %d\n", errno);
    }
}
