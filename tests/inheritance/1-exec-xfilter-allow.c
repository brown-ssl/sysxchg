#include "../common.h"

int main(void)
{
    TEST_DESCRIBE("Testing exec xfilter with allowed syscall");
    TEST_XFILTER_EXPECT_ALLOWED(syscall(SYS_getuid), "getuid", SYS_getuid);
}
