#include "../common.h"

int main(void)
{
    TEST_XFILTER_EXPECT_ALLOWED(syscall(SYS_getgid), "getgid", SYS_getgid);
    TEST_XFILTER_EXPECT_BLOCKED(syscall(SYS_geteuid), "geteuid", SYS_geteuid);
    TEST_XFILTER_EXPECT_BLOCKED(syscall(SYS_getuid), "getuid", SYS_getuid);
}
