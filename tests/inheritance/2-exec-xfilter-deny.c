#include "../common.h"

int main(void)
{
    TEST_DESCRIBE("Testing exec xfilter with blocked syscall");
    TEST_XFILTER_EXPECT_BLOCKED(syscall(SYS_getuid), "getuid", SYS_getuid);
}
