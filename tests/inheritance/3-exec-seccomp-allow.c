#include "../common.h"

INIT_SECCOMP_FLAG();

int main(void)
{
    TEST_DESCRIBE("Testing exec seccomp with allowed syscall");
    INIT_SIGNAL_HANDLER_EXPECT_ALLOWED();
    syscall(SYS_getuid);
    TEST_SECCOMP_EXPECT_ALLOWED("getuid", SYS_getuid);
}
