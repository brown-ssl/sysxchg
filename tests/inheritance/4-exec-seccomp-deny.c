#include "../common.h"

INIT_SECCOMP_FLAG();

int main(void)
{
    TEST_DESCRIBE("Testing exec seccomp with blocked syscall");
    INIT_SIGNAL_HANDLER_EXPECT_BLOCKED();
    syscall(SYS_getuid);
    TEST_SECCOMP_EXPECT_BLOCKED("getuid", SYS_getuid);
}