#include <unistd.h>
#include "../common.h"

int main(void)
{
    TEST_DESCRIBE("Testing exec xfilter -> exec xfilter");
    TEST_XFILTER_EXPECT_ALLOWED(syscall(SYS_getgid), "getgid", SYS_getgid);
    TEST_XFILTER_EXPECT_ALLOWED(syscall(SYS_geteuid), "geteuid", SYS_geteuid);
    TEST_XFILTER_EXPECT_BLOCKED(syscall(SYS_getuid), "getuid", SYS_getuid);
    fflush(stdout);

    char *prog = "./inheritance/bin/7-exec-xfilter-2";
    int ret = execve(prog, (char *[]){prog, 0}, NULL);
    if (ret)
    {
        printf("execve returned %d\n", ret);
        printf("errno: %d\n", errno);
    }
}
