#ifndef __COMMON_H__
#define __COMMON_H__

#include <errno.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syscall.h>
#include <unistd.h>

#ifndef __FILE_NAME__
#define __FILE_NAME__ \
  (strrchr(__FILE__, '/') ? strrchr(__FILE__, '/') + 1 : __FILE__)
#endif

/*
 * general pass/fail macro
 */
#define TEST_EXPECT_SUCCESS(retval) \
  (((retval) >= 0) ? printf("[PASS] %s\n", __FILE_NAME__) \
                   : printf("[FAIL][expected success] %s\t(%s:%d)\n", \
                              __FILE_NAME__, __func__, __LINE__))

#define TEST_EXPECT_FAILURE(retval) \
  (((retval) < 0) ? printf("[PASS] %s\n", __FILE_NAME__) \
                   : printf("[FAIL][expected failure] %s\t(%s:%d)\n", \
                              __FILE_NAME__, __func__, __LINE__))

#define TEST_WARN(msg) \
  printf("[WARN][" msg "] %s\t(%s:%d)\n", \
                              __FILE_NAME__, __func__, __LINE__)

#define TEST_DESCRIBE(msg) \
  printf("[Description] %s\n\n", msg)

/*
 * xfilter just returns an error code upon bad syscall
 */
#define TEST_XFILTER_EXPECT_BLOCKED(retval, syscall, nr)                                        \
  (((retval) == -1) ? printf("[PASS][syscall %s:#%d blocked] %s\n", syscall, nr, __FILE_NAME__) \
                    : printf("[FAIL][syscall %s:#%d allowed] %s\t(%s:%d)\n",                    \
                             syscall, nr, __FILE_NAME__, __func__, __LINE__))

#define TEST_XFILTER_EXPECT_ALLOWED(retval, syscall, nr)                                        \
  (((retval) != -1) ? printf("[PASS][syscall %s:#%d allowed] %s\n", syscall, nr, __FILE_NAME__) \
                    : printf("[FAIL][syscall %s:#%d blocked] %s\t(%s:%d)\n",                    \
                             syscall, nr, __FILE_NAME__, __func__, __LINE__))

/*
 * SeccompBPF signals SIGSYS upon bad syscall
 */

/* 0 = block, 1 = allowed */
#define INIT_SECCOMP_FLAG() volatile sig_atomic_t flag

extern volatile sig_atomic_t flag __attribute__((weak));

void signal_handler_expect_blocked(int signo)
{
  /* this isn't async-signal-safe, but that shouldn't really matter to us */
  flag = 0;
}

#define INIT_SIGNAL_HANDLER_EXPECT_BLOCKED() ({                 \
  flag = 1;                                                     \
  if (signal(SIGSYS, signal_handler_expect_blocked) == SIG_ERR) \
    fprintf(stderr,                                             \
            "[ERROR][unable to catch SIGSYS] %s\t(%s:%d)\n",    \
            __FILE_NAME__, __func__, __LINE__);                 \
})

#define TEST_SECCOMP_EXPECT_BLOCKED(syscall, nr) ({                            \
  if (flag)                                                                    \
    printf("[FAIL][syscall %s:#%d allowed] %s\n", syscall, nr, __FILE_NAME__); \
  else                                                                         \
    printf("[PASS][syscall %s:#%d blocked] %s\n", syscall, nr, __FILE_NAME__); \
})

void signal_handler_expect_allowed(int signo)
{
  /* this isn't async-signal-safe, but that shouldn't really matter to us */
  flag = 0;
}

#define INIT_SIGNAL_HANDLER_EXPECT_ALLOWED() ({                 \
  flag = 1;                                                     \
  if (signal(SIGSYS, signal_handler_expect_allowed) == SIG_ERR) \
    fprintf(stderr,                                             \
            "[ERROR][unable to catch SIGSYS] %s\t(%s:%d)\n",    \
            __FILE_NAME__, __func__, __LINE__);                 \
})

#define TEST_SECCOMP_EXPECT_ALLOWED(syscall, nr) ({                            \
  if (flag)                                                                    \
    printf("[PASS][syscall %s:#%d allowed] %s\n", syscall, nr, __FILE_NAME__); \
  else                                                                         \
    printf("[FAIL][syscall %s:#%d blocked] %s\n", syscall, nr, __FILE_NAME__); \
})

#endif /* __COMMON_H__ */
