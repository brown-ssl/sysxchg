#!/bin/bash

INHERITANCE_TESTS=$(dirname "${0}")"/inheritance/bin/*"
EXCHANGE_TESTS=$(dirname "${0}")"/exchange/bin/*-1"

if [ "$1" = "inheritance" ]; then
	echo "============================="
	echo "====[ Inheritance Tests ]===="
	echo "============================="
	counter=1
	dmesg -n 3
	for t in $INHERITANCE_TESTS; do
		[[ $t == *-2 ]] && continue
		echo "--[ Test $counter: $t"
		$t
		echo
		((counter++))
	done
	dmesg -n 4
	exit 0
elif [ "$1" = "exchange" ]; then
	echo "======================"
	echo "====[ Xchg Tests ]===="
	echo "======================"
	counter=1
	dmesg -n 3
	for t in $EXCHANGE_TESTS; do
		echo "--[ Test $counter: $t"
		$t
		echo
		((counter++))
	done
	dmesg -n 4
	exit 0
else
	echo "Usage: $0 [inheritance|exchange]"
fi
