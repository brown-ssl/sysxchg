#!/usr/bin/env bash
# Calculate sort twice

IN=${IN:-$PASH_TOP/oneliners/input/1M.txt}

cat $IN | tr A-Z a-z | sort | sort -r
