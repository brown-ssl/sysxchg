#!/usr/bin/env bash
# Match complex regular-expression over input

IN=${IN:-$PASH_TOP/oneliners/input/1M.txt}

cat $IN | tr A-Z a-z | grep '\(.\).*\1\(.\).*\2\(.\).*\3\(.\).*\4'
