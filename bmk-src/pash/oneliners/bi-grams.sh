#!/usr/bin/env bash
# Find all 2-grams in a piece of text

IN=${IN:-$PASH_TOP/oneliners/input/1M.txt}

. $PASH_TOP/oneliners/bi-gram.aux.sh

cat $IN |
  tr -cs A-Za-z '\n' |
  tr A-Z a-z |
  bigrams_aux |
  sort |
  uniq


