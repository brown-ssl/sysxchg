#!/bin/bash

source_var() {
  export FROM=2015
  export TO=2015
  if [[ "$1" == "--trace" ]]; then
    export IN=file://$PASH_TOP/evaluation/benchmarks/max-temp/input/trace
  else
    export IN=file://$PASH_TOP/evaluation/benchmarks/max-temp/input
  fi
}
