# SysXCHG Kernel Configurations

 - `linux-6.0.8-vanilla.config`: Linux kernel default (baseline) configuration
 - `linux-6.0.8-vanilla-dbg.config`: Linux kernel default (baseline) debug configuration
 - `linux-6.0.8-inheritance.config`: Linux kernel inheritance model configuration (exec filter and xfilter support)
 - `linux-6.0.8-inheritance-dbg.config`: Linux kernel inheritance model debug configuration
 - `linux-6.0.8-exchange.config`: Linux kernel exchange model configuration (full SysXCHG)
 - `linux-6.0.8-exchange-dbg.config`: Linux kernel exchange model debug configuration
 - `sysxchg-cpu2017.cfg`: SPEC CPU 2017 configuration
