#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <unistd.h>

#define SHM_SIZE  sizeof(struct timeval)

int main(int argc, char *argv[], char *envp[]) {
    int shmid;
    struct timeval *start_time;
    double elapsed_time;
    pid_t pid;
    int status;

    if (argc == 1) {
        printf("[Error] Pass a program to run!\n");
        exit(EXIT_FAILURE);
    }

    /* Create shared memory segment */
    shmid = shmget(IPC_PRIVATE, SHM_SIZE, IPC_CREAT | 0666);
    if (shmid < 0) {
        perror("shmget");
        exit(EXIT_FAILURE);
    }

    /* Attach shared memory segment */
    start_time = (struct timeval*)shmat(shmid, NULL, 0);
    if (start_time == (void *)-1) {
        perror("shmat");
        exit(EXIT_FAILURE);
    }

    /* Fork a child process */
    pid = fork();

    if (pid < 0) {
        /* Fork failed */
        perror("fork");
        exit(EXIT_FAILURE);

    } else if (pid == 0) {
        /* Child process */

        /* Attach shared memory segment */
        start_time = (struct timeval*)shmat(shmid, NULL, 0);
        if (start_time == (void *)-1) {
            perror("shmat");
            exit(EXIT_FAILURE);
        }

        /* Get the child start time */
        gettimeofday(start_time, NULL);

        /* Execute the timed program */
        char *child_argv[] = { argv[1], argv[2], argv[3], NULL };
        execve(child_argv[0], child_argv, envp);
        perror("execve");
        exit(EXIT_FAILURE);

    } else {
        /* Parent process */
        wait(&status);

        if (WIFEXITED(status) && WEXITSTATUS(status) == 0) {
            /* Script executed successfully */
            struct timeval end_time;
            gettimeofday(&end_time, NULL);
            elapsed_time = (end_time.tv_sec - start_time->tv_sec) +
                ((end_time.tv_usec - start_time->tv_usec) / 1000000.0);
            fprintf(stderr, "Time: %f (seconds)\n", elapsed_time);

        } else {
            /* Script failed to execute */
            printf("[Error] Script failed to execute\n");
        }

        /* Detach shared memory segment */
        shmdt(start_time);

        /* Delete shared memory segment */
        shmctl(shmid, IPC_RMID, NULL);
    }

    return 0;
}

