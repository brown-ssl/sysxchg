#!/bin/bash

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

# build the rootfs
build_rootfs()
{
    mkdir -p "${BUILD_DIR}/rootfs"
    cd "${BUILD_DIR}/rootfs"

    if [ ! -e 'bullseye.img' ]; then
        wget https://raw.githubusercontent.com/google/syzkaller/master/tools/create-image.sh
        chmod +x create-image.sh

        local pkgs="make,git,vim,ima-evm-utils,keyutils,openssl,python3,build-essential,xz-utils,unzip,zip,patchelf,tcl,bsdextrautils,file,tcpdump,locales,perl,cpanminus,gawk,strace,moreutils,calc,attr"
        ADD_PACKAGE="${pkgs}" \
            ./create-image.sh -a x86_64 -d bullseye -f full -s 51200
    fi

    # mount the rootfs image
    sudo mkdir -p "${MOUNT_DIR}"
    sudo mount -o loop "${ROOTFS_IMG}" "${MOUNT_DIR}"

    # copy the necessary stuff inside
    sudo cp -r \
        "${BMK_SRC_DIR}" \
        "${CONFIG_DIR}" \
        "${POLICY_DIR}" \
        "${TEST_DIR}" \
        "${TOOL_DIR}" \
        "${MOUNT_DIR}/root"
    sudo mkdir -p "${MOUNT_DIR}/root/build"

    # create and copy-over keys
    ${TOOL_DIR}/create-keys.sh
    sudo mkdir -p "${MOUNT_DIR}/etc/keys"
    sudo cp "${BUILD_DIR}"/keys/* "${MOUNT_DIR}/etc/keys/"

    # unmount the rootfs image
    sudo umount "${MOUNT_DIR}"
}

# build the kernel
build_kernel()
{
    local config_name="${1}"
    local kernel_config="${CONFIG_DIR}/${KERNEL_VERSION}-${config_name}.config"
    local build_dir="${BUILD_DIR}/${KERNEL_VERSION}-${config_name}"

    mkdir -p "${build_dir}"

    cd "${KERNEL_SRC_DIR}"
    cp "${kernel_config}" "${build_dir}/.config"
    make O="${build_dir}" -j"$(nproc)"

    vmstart "${config_name}"
    vmcmd "sed -i 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen"
    vmcmd "locale-gen"
    vmstop
}

build_tests()
{
    vmstart vanilla
    vmcmd "make clean --directory=${VM_TEST_DIR}"
    vmcmd "make inheritance --directory=${VM_TEST_DIR}"
    vmstop

    vmstart exchange ima_measure
    vmcmd "make exchange --directory=${VM_TEST_DIR}"
    vmstop
}

build_benchmark()
{
    local bmk="${1}"

    case "${bmk}" in
    
        spec )
            # do setup in vanilla kernel
            vmstart vanilla

            # install SPEC
            vmcmd "${SPEC_SCRIPT} --install"

            # build SPEC for each filter type
            for ftype in ${INHERITANCE_FILTER_TYPES}; do
                vmcmd "${SPEC_SCRIPT} --filter ${ftype} --build"
                vmcmd "${SPEC_SCRIPT} --filter ${ftype} --enforce"
                vmcmd "${SPEC_SCRIPT} --filter ${ftype} --verify"
            done

            # shutdown vanilla kernel
            vmstop
            ;;

        sqlite )
            # do setup in vanilla kernel
            vmstart vanilla

            # install/unpack SQLite
            vmcmd "${SQLITE_SCRIPT} --install"

            # build SQLite for each filter type
            vmcmd "${SQLITE_SCRIPT} --build"
            vmcmd "${SQLITE_SCRIPT} --enforce"
            vmcmd "${SQLITE_SCRIPT} --verify"

            # shutdown vanilla kernel
            vmstop
            ;;

        pash )
            # do setup in vanilla kernel w/ IMA measurement mode
            vmstart vanilla ima_measure

            # install/unpack PaSH
            vmcmd "${PASH_SCRIPT} --install"

            # build PaSH for each filter type
            vmcmd "${PASH_SCRIPT} --filter xfilter --enforce"
            vmcmd "${PASH_SCRIPT} --filter xfilter --verify"
            vmcmd "${PASH_SCRIPT} --filter seccomp --enforce"
            vmcmd "${PASH_SCRIPT} --filter seccomp --verify"

            # shutdown vanilla kernel
            vmstop
            ;;

        * )
            do_error "Invalid benchmark: ${bmk}."
            ;;
    esac
}

print_usage()
{
    cat 1>&2 <<EOF
Usage: $(ps -o args= ${PPID} | cut -d' ' -f2) build [options]

Options:
  help              print this help menu
  all               build all kernels and rootfs
  kernel <config>   build kernel with config <config>
  rootfs            build rootfs, including all tests and benchmarks
  tests             build all tests
  benchmark <bmk>   build benchmark <bmk>

Configs <config>:
  vanilla           Linux kernel default (baseline) configuration
  inheritance       Linux kernel inheritance model configuration (exec filter and xfilter support)
  exchange          Linux kernel exchange model configuration (full SysXCHG)

Benchmarks <bmk>:
  spec              SPEC CPU 2017
  sqlite            SQLite Speedtest
  pash              a subset of PaSH benchmarks
EOF
}

main()
{
    local config bmk

    # check for command line arguments
    if [[ ${#} == 0 ]]; then
        print_usage
        exit 1
    fi

    case "${1}" in
        all )
            build_rootfs
            for config in ${KERNEL_CONFIGS}; do
                build_kernel "${config}"
            done
            build_tests
            for bmk in ${BENCHMARKS}; do
                build_benchmark "${bmk}"
            done
            ;;

        kernel )
            config="${2}"
            verify_config_opt "${config}"
            build_kernel "${config}"
            ;;

        rootfs )
            build_rootfs
            ;;

        tests )
            build_tests
            ;;

        benchmark )
            bmk="${2}"
            verify_bmk_opt "${bmk}"
            build_benchmark "${bmk}"
            ;;

        help )
            print_usage
            ;;

        * )
            do_error "Invalid argument: ${1}."
            ;;
    esac
}
main ${@}
