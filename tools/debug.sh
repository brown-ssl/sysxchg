#!/bin/bash

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

print_usage()
{
    cat 1>&2 <<EOF
Usage: $(ps -o args= ${PPID} | cut -d' ' -f2) debug [options]

Options:
  help              print this help menu
  vanilla           drop into a vanilla kernel VM
  vanilla-dbg       drop into a vanilla debug kernel VM
  inheritance       drop into an inheritance kernel VM
  inheritance-dbg   drop into an inheritance debug kernel VM
  exchange          drop into an exchange kernel VM
  exchange-dbg      drop into an exchange debug kernel VM
EOF
}

main()
{
    # check for command line arguments
    if [[ ${#} == 0 ]]; then
        print_usage
        exit 1
    fi

    local key="${1}"
    shift
    case "${key}" in
        vanilla )
            vmstart vanilla debug ${@}
            ;;

        vanilla-dbg )
            vmstart vanilla-dbg debug ${@}
            ;;

        inheritance )
            vmstart inheritance debug ${@}
            ;;

        inheritance-dbg )
            vmstart inheritance-dbg debug ${@}
            ;;

        exchange )
            vmstart exchange debug ${@}
            ;;

        exchange-dbg )
            vmstart exchange-dbg debug ${@}
            ;;

        help )
            print_usage
            ;;

        * )
            do_error "Invalid argument: ${key}."
            ;;
    esac
}
main ${@}
