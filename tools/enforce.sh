#!/bin/bash
set -euo pipefail

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

BUILD_FILTER_XFILTER="${ENFORCEMENT_DIR}/xfilter.py"
BUILD_FILTER_SECCOMP="${ENFORCEMENT_DIR}/seccomp.py"

# parameters/defaults 
BIN=""
WORK_DIR="${BUILD_DIR}/filters"
FILTER="${BUILD_DIR}/filter"
POLICIES=()
FILTER_TYPE="xfilter"
INSTALL_TYPE="embed"

usage="Usage: ${0} [options] binary [policies...]

Options:
  --help         display this menu
  --seccomp      use a seccomp filter (default: xfilter)
  --lib <dir>    install filter as a library in <dir> (default: embed)"


# Create and embed a per-process syscall filter in the output binary
do_xfilter()
{
	# Populate filter
	${BUILD_FILTER_XFILTER} "${POLICIES[@]}" > "${FILTER}"

	if [[ ${INSTALL_TYPE} == "embed" ]]; then
		# Create a new binary with a filter section
		objcopy --add-section .xfilter="${FILTER}"					\
						--set-section-flags .xfilter=noload,readonly	\
						"${BIN}" "${BIN}"
	else
		# Create library-based filter and attach it to the binary
		local filter_src
		local filter_lib

		filter_src="${LIB_DIR}/filter_$(basename "${BIN}").c"
		filter_lib="${LIB_DIR}/libxfilter-$(basename "${BIN}").so"

		# Populate filter
		${BUILD_FILTER_XFILTER} --lib "${POLICIES[@]}" > "${filter_src}"
		
		# Compile filter-installing library
		gcc -ggdb3 -fPIC -shared -o "${filter_lib}" "${filter_src}"
		
		# Patch the input binary so that it loads the filter-installing library
		patchelf --add-needed "${filter_lib}" "${BIN}"

		# Clean up some generated files
		rm "${filter_src}" "${FILTER}"
	fi
}

# Create and embed a seccomp filter in the output binary
do_seccomp()
{
	filter_src="filter_$(basename "${BIN}").c"
	filter_exe="filter_$(basename "${BIN}")"

	# Populate filter
	${BUILD_FILTER_SECCOMP} "${POLICIES[@]}" > "${filter_src}"
	
	# Compile filter-writing executable
	gcc -ggdb3 -fPIC -o "${filter_exe}" "${filter_src}"
	
	# Write filter to file
	./"${filter_exe}" "${FILTER}"

	# Create a new binary with a seccomp section
	objcopy --add-section .seccomp="${FILTER}"		\
		--set-section-flags .seccomp=noload,readonly	\
		"${BIN}" "${BIN}"

	# Clean up generated files
	rm "${filter_src}" "${filter_exe}" "${FILTER}"
}

main()
{
	if [[ ${#} -lt 1 ]]; then
		echo "${usage}"
		exit 1
	fi

	# Parse arguments
	local positional=()
	while [[ ${#} -gt 0 ]]; do
		key=${1}
		case ${key} in
			-h | --help)
				echo "${usage}"
				exit 0
				;;
			--seccomp)
				FILTER_TYPE="seccomp"
				shift
				;;
			--lib)
				INSTALL_TYPE="lib"
				LIB_DIR="${2}"
				shift
				shift
				;;
			*)
				positional+=("${1}")
				shift
		esac
	done
	set -- "${positional[@]}"

	BIN=${1}
	shift
	POLICIES=("${@}")

	# argument validation
	if [[ ${FILTER_TYPE} == "seccomp" && ${INSTALL_TYPE} == "lib" ]]; then
		do_error "This script does not support attaching seccomp-filter-" \
			"installing libraries to binaries. See sysfilter for that."
	fi

	# setup working directory
	mkdir -p "${WORK_DIR}"

	# Create and embed the filter
	if [[ ${FILTER_TYPE} == "xfilter"  ]]; then
		do_xfilter
	elif [[ ${FILTER_TYPE} == "seccomp" ]]; then
		do_seccomp
	fi
}
main ${@}
