#!/bin/bash

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

SPEC_TARXZ="${BMK_SRC_DIR}/cpu2017-1.1.0.tar.xz"
SPEC_SRC_DIR="${BUILD_DIR}/cpu2017"

# SPEC CPU 2017 C/C++ benchmarks
BMKS=" \
600.perlbench_s \
602.gcc_s \
605.mcf_s \
620.omnetpp_s \
623.xalancbmk_s \
625.x264_s \
631.deepsjeng_s \
641.leela_s \
657.xz_s \
619.lbm_s \
638.imagick_s \
644.nab_s"

# Paths to SPEC CPU 2017 C/C++ benchmarks
BINS=" \
    benchspec/CPU/600.perlbench_s/exe/perlbench_s_base.__TYPE__-m64 \
    benchspec/CPU/602.gcc_s/exe/sgcc_base.__TYPE__-m64 \
    benchspec/CPU/605.mcf_s/exe/mcf_s_base.__TYPE__-m64 \
    benchspec/CPU/620.omnetpp_s/exe/omnetpp_s_base.__TYPE__-m64 \
    benchspec/CPU/623.xalancbmk_s/exe/xalancbmk_s_base.__TYPE__-m64 \
    benchspec/CPU/625.x264_s/exe/imagevalidate_625_base.__TYPE__-m64 \
    benchspec/CPU/625.x264_s/exe/ldecod_s_base.__TYPE__-m64  \
    benchspec/CPU/625.x264_s/exe/x264_s_base.__TYPE__-m64 \
    benchspec/CPU/631.deepsjeng_s/exe/deepsjeng_s_base.__TYPE__-m64 \
    benchspec/CPU/641.leela_s/exe/leela_s_base.__TYPE__-m64 \
    benchspec/CPU/657.xz_s/exe/xz_s_base.__TYPE__-m64 \
    benchspec/CPU/619.lbm_s/exe/lbm_s_base.__TYPE__-m64 \
    benchspec/CPU/638.imagick_s/exe/imagevalidate_638_base.__TYPE__-m64 \
    benchspec/CPU/638.imagick_s/exe/imagick_s_base.__TYPE__-m64 \
    benchspec/CPU/644.nab_s/exe/nab_s_base.__TYPE__-m64"

# user-provided arguments
FILTER_TYPE="vanilla"
ACTION=""
ITERS=10
SIZE="test"

do_install()
{
    # setup source
    # mkdir -p "${SPEC_SRC_DIR}"
    # tar -xf "${SPEC_TARXZ}" -C "${SPEC_SRC_DIR}"
    cp -r "${BMK_SRC_DIR}/cpu2017" "${SPEC_SRC_DIR}"
    cd "${SPEC_SRC_DIR}"

    # install tools etc.
    ./install.sh -f

    # create separate configs for separate variants
    cp "${CONFIG_DIR}"/sysxchg-cpu2017.cfg "${SPEC_SRC_DIR}"/config/vanilla.cfg
    cp "${CONFIG_DIR}"/sysxchg-cpu2017.cfg "${SPEC_SRC_DIR}"/config/xfilter.cfg
    cp "${CONFIG_DIR}"/sysxchg-cpu2017.cfg "${SPEC_SRC_DIR}"/config/seccomp.cfg
    sed -i 's/__LABEL__/vanilla/' "${SPEC_SRC_DIR}"/config/vanilla.cfg
    sed -i 's/__LABEL__/xfilter/' "${SPEC_SRC_DIR}"/config/xfilter.cfg
    sed -i 's/__LABEL__/seccomp/' "${SPEC_SRC_DIR}"/config/seccomp.cfg

    do_log "Installed SPEC CPU 2017."
}

do_build()
{
    cd "${SPEC_SRC_DIR}"

    source shrc
    runcpu \
        --config="${FILTER_TYPE}" \
        --action=build \
        --rebuild \
        --noreportable \
        ${BMKS}

    do_log "Done building ${FILTER_TYPE} benchmarks for SPEC CPU 2017."
}

do_benchmark()
{
    cd "${SPEC_SRC_DIR}"

    source shrc
    runcpu \
        --config="${FILTER_TYPE}" \
        --action=run \
        --nobuild \
        --parallel_test="$(nproc)" \
        --size="${SIZE}" \
        --iterations="${ITERS}" \
        --noreportable \
        ${BMKS}

    cd result

    cp "$(ls -1rt *.intspeed.*.txt | tail -1)" \
        "/tmp/cpu2017-intspeed-${FILTER_TYPE}.log"
    cp "$(ls -1rt *.fpspeed.*.txt | tail -1)" \
        "/tmp/cpu2017-fpspeed-${FILTER_TYPE}.log"

    do_log "Done running ${FILTER_TYPE} benchmarks for SPEC CPU 2017" \
        "(iters=${ITERS}, size=${SIZE})."
}

do_clean()
{
    read -p "Are you sure you want to clean SPEC CPU 2017? " -n 1 -r
    echo
    if [[ ${REPLY} =~ ^[Yy]$ ]]; then
        cd "${SPEC_SRC_DIR}"

        # clean up build and run directories
        source shrc
        runcpu --deletework || true
        runcpu --config="${FILTER_TYPE}" -a clean all
        runcpu --config="${FILTER_TYPE}" -a trash all
        runcpu --config="${FILTER_TYPE}" -a clobber all

        read -p "Delete previous results (i.e., result/*)? " -n 1 -r
        echo
        if [[ ${REPLY} =~ ^[Yy]$ ]]; then
            rm -fv result/*
        fi

        do_log "Done cleaning up SPEC CPU 2017 for ${FILTER_TYPE} filter type."
    fi
}

do_enforce()
{
    local bins="${BINS//__TYPE__/${FILTER_TYPE}}"

    cd "${SPEC_SRC_DIR}"

    do_enforce_common "${FILTER_TYPE}" "${bins}"

    do_log "Done enforcing SPEC CPU 2017 for ${FILTER_TYPE} filter type."
}

do_verify()
{
    local bins="${BINS//__TYPE__/${FILTER_TYPE}}"

    cd "${SPEC_SRC_DIR}"

    if do_verify_filters "${FILTER_TYPE}" "${bins}"; then
        do_log "Verification of SPEC CPU 2017 binaries successful " \
            "for ${FILTER_TYPE} filter type."
    else
        do_warn "Verification of SPEC CPU 2017 binaries successful " \
            "for ${FILTER_TYPE} filter type."
    fi
}

print_usage()
{
    cat 1>&2 <<EOF
Usage: ${0} [options]

Options:
  --install             install SPEC CPU 2017
  --filter <type>       specify filter type (default: vanilla)
  --build               build benchmarks for a specific filter type
  --enforce             enforce benchmarks with a specific type of filter
  --verify              check benchmarks are enforced with a filter type
  --benchmark           run benchmarks of a specific filter type
  --iters N             benchmark iterations (default: 5)
  --size <test | ref>   benchmark size (default: test)
  --clean               clean up for a given filter type
  -h, --help            print this help menu

Filter types:
  vanilla   no enforcement (default)
  xfilter   syscall filtering with xfilter
  seccomp   syscall filtering with seccomp
EOF
}

main()
{
    # check for command line arguments
    if [[ ${#} == 0 ]]; then
        print_usage
        exit 1
    fi

    # Check if the user provided SPEC; we cannot because of licensing issues.
    if [[ ! -d "${BMK_SRC_DIR}/cpu2017" ]]; then
        do_error "SPEC CPU 2017 source code not provided. See README.md for details."
    fi

    # parse command-line arguments
    while [[ ${#} -gt 0 ]]; do

        case "${1}" in
            --install   ) ACTION="do_install"   ;;
            --filter    ) FILTER_TYPE="${2}"; shift  ;;
            --build     ) ACTION="do_build"     ;;
            --enforce   ) ACTION="do_enforce"   ;;
            --verify    ) ACTION="do_verify"    ;;
            --benchmark ) ACTION="do_benchmark" ;;
            --iters     ) ITERS="${2}"; shift   ;;
            --size      ) SIZE="${2}"; shift    ;;
            --clean     ) ACTION="do_clean"     ;;
            -h | --help ) ACTION="print_usage"  ;;
            *           ) do_error "Invalid argument: ${1}." ;;
        esac
        shift

    done

    # argument validation
    [[ -z ${ACTION} ]] && do_error "No action provided."

    [[ ${FILTER_TYPE} != "vanilla"
        && ${FILTER_TYPE} != "xfilter"
        && ${FILTER_TYPE} != "seccomp" ]] && do_error "Invalid filter type."

    [[ ${ACTION} == "do_benchmark"
        && ${SIZE} != "test"
        && ${SIZE} != "ref" ]] && do_error "Invalid size."

    [[ ${ACTION} == "do_benchmark"
        && ${ITERS} -lt 1 ]] && do_error "Invalid size."

    # do the action
    ${ACTION}
}
main ${@}
