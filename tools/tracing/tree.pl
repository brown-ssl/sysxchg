#!/usr/bin/perl -w

use strict;

use File::Basename;
use Term::ANSIColor qw(:constants);
use Storable qw(dclone);
use Tree::Simple;
use List::Util 'uniq';

my %pid_map = ();
my $tree;
my @all_progs = ();
my @all_chains = ();

sub dolog
{
	my ($level, $msg) = @_;

	if ($level eq "DBG") {
		print STDERR CYAN, "[DBG] ", RESET, "$msg\n";
	} elsif ($level eq "INFO") {
		print BLUE, "[INFO] ", RESET, "$msg\n";
	} elsif ($level eq "WARN") {
		print STDERR YELLOW, "[WARNING] ", RESET, "$msg\n";
	} elsif ($level eq "ERR") {
		print STDERR RED, "[ERROR] ", RESET, "$msg\n";
	}
}

sub handle_process_creation
{
	my ($pid, $ppid, $time) = @_;
	my ($node, $subtree, $prog);

	# This function is called for clone/[v]fork, so initially the program
	# name is the same as the parent process' most recent program
	$prog = $pid_map{$ppid}[-1]->getNodeValue()->{PROG};

	# Construct a node that describes a PID
	$node = {};
	$node->{PID}		= $pid;  # this node's PID
	$node->{PPID}		= $ppid; # parent's PID
	$node->{PROG}		= $prog; # program name
	$node->{CTIME}	= $time; # time of clone/[v]fork
	$node->{ETIME}	= 0;     # time of execve[at]

	# Create a new subtree from the node
	$subtree = Tree::Simple->new($node);

	# Join the parent and the current node
	$pid_map{$ppid}[-1]->addChild($subtree);

	# Associate the PID with its subtree
	push(@{$pid_map{$pid}}, $subtree);

	dolog("DBG",
		"FORK: { pid = $pid, ppid = $ppid, prog = $prog, ctime = $time, etime = 0 }");
}

sub is_script
{
	my ($file) = @_;
	my $type = `file -bL $file`;
	return ($type =~ /script/i);
}

sub get_interp
{
	my ($file) = @_;
	my ($line, $interp);

	unless (open(SCRIPT, $file)) {
		# Just return the file name if we can't open it to prevent cases like
		# files built, run, and rm'ed by Configure from breaking things
		dolog("WARN", "Failed to open $file.\n");
		return $file;
	}

	dolog("DBG", "Searching for interpreter for $file");

	# Get the first line of script
	$line = <SCRIPT>;

	# Match on the shebang line to get interpreter
	$interp = $file;
	if ($line =~ /^#!\s*(?:\S+\/env\s+)?(\S+)(?:\s+([-\/\w.]+))?/) {
		$interp = `which $1`;
		chomp $interp;

		if ($interp eq "") {
			$interp = $1;
			dolog("WARN", "Interpreter ($interp) not found in path, proceeding anyway...");
		}
		
		dolog("DBG", "Using interpreter: $file => $interp");
	} else {
		dolog("DBG", "No interpreter found for $file");
	}

	return $interp;
}

sub handle_program_exec
{
	my ($pid, $time, $prog) = @_;
	my ($old_node, $node, $subtree, $mod_prog, $ppid, $ctime);

	# Clone node corresponding to PID
	if (!$pid_map{$pid}[-1]) {
		dolog("ERR", "Trace file not sorted chronologically (try 'sort -k2,2 -g')");
		exit 1;
	}
	$old_node = $pid_map{$pid}[-1]->getNodeValue();
	$node = dclone($old_node);

	$ppid  = $node->{PPID};
	$ctime = $node->{CTIME};

	# Get any interpreter and add it to the list of all programs too
	$mod_prog = $prog;
	if (is_script($prog)) {
		$mod_prog = get_interp($prog);
		push(@all_progs, $mod_prog);
	}

	# Log the executed program (non-modified name)
	push(@all_progs, $prog);

	# Update fields of new node
	$node->{PROG}  = $mod_prog;
	$node->{ETIME} = $time;

	# Create a new subtree from node
	$subtree = Tree::Simple->new($node);

	# Associate the PID with its subtree
	push(@{$pid_map{$pid}}, $subtree);

	dolog("DBG",
		"EXEC: { pid = $pid, ppid = $ppid, prog = $prog, ctime = $ctime, etime = $time }");
}

sub handle_first_program_exec
{
	my ($pid, $time, $prog) = @_;
	my ($node, $mod_prog);

	# Get any interpreter and add it to the list of all programs too
	$mod_prog = $prog;
	if (is_script($prog)) {
		$mod_prog = get_interp($prog);
		push(@all_progs, $mod_prog);
	}

	# Log the executed program (non-modified name)
	push(@all_progs, $prog);

	# Construct a node that describes a PID
	$node = {};
	$node->{PID}		= $pid;				# this node's PID
	$node->{PPID}		= 0;					# parent's PID
	$node->{PROG}		= $mod_prog;	# program name
	$node->{CTIME}	= 0;					# time of clone/[v]fork
	$node->{ETIME}	= $time;			# time of execve[at]

	# Create a new subtree from the node
	$tree = Tree::Simple->new($node);

	# Associate the PID with its subtree
	push(@{$pid_map{$pid}}, $tree);

	dolog("DBG",
		"EXEC: { pid = $pid, ppid = 0, prog = $prog, ctime = 0, etime = $time }");
}

sub parse_trace_file
{
	my $trace_file = $_[0];
	my ($line, $pid, $cpid, $time, $prog);

	open(TRACE, $trace_file) or die "[Error] Failed to open $trace_file: $!.\n";

	while ($line = <TRACE>) {
		# Extract root PID
		if ($. == 1) {
			if (($pid, $time, $prog) =
					($line =~ /^([0-9]*)\s*([0-9]*\.[0-9]*)\s*\[\s*59\].*\("(.*)", \[/)) {
				handle_first_program_exec($pid, $time, $prog);
				next;
			} else {
				dolog("WARN", "Trace file does not start with an execve.");
			}
		}

		# Match clone, fork, and vfork lines
		if (($pid, $time, $cpid) =
				($line =~ /^([0-9]*)\s*([0-9]*\.[0-9]*)\s*\[\s*5[6-8]\].* = ([0-9]*)$/)) {
			handle_process_creation($cpid, $pid, $time);
		}

		# Match execve lines (program name is first argument)
		if (($pid, $time, $prog) =
				($line =~ /^([0-9]*)\s*([0-9]*\.[0-9]*)\s*\[\s*59\].*\("(.*)", \[/)) {
			handle_program_exec($pid, $time, $prog);
		}

		# Match execveat lines (program name is second argument)
		if (($pid, $time, $prog) =
				($line =~ /^([0-9]*)\s*([0-9]*\.[0-9]*)\s*\[\s*322\].*\(.*, "(.*)", \[/)) {
			handle_program_exec($pid, $time, $prog);
		}
	}

	close(TRACE);
}

sub is_duplicate
{
	my ($f1, $f2) = @_;
	my ($i1, $i2);

	($i1) = (`stat $f1 2>/dev/null` =~ /Inode: (\d+)/);
	($i2) = (`stat $f2 2>/dev/null` =~ /Inode: (\d+)/);

	# Go off of name since we presumably can't find the file
	if ((!defined $i1) || (!defined $i2)) { return ($f1 eq $f2); }
	# Go off of inode since we can stat the files
	else { return ($i1 == $i2); }
}

sub get_chains
{
	my ($_tree, @progs) = @_;
 	my ($idx, $node, $adjtree, $child);
	my (@pid_arr);

	$idx = 0;
	@pid_arr = @{$pid_map{$_tree->getNodeValue()->{PID}}};
	while ($pid_arr[$idx]->getUID() ne $_tree->getUID()) { $idx++; }

	foreach $adjtree (@pid_arr[$idx..$#pid_arr]) {
		$node = $adjtree->getNodeValue();

		# Remove adjacent duplicates while adding to progs list
		if (!@progs) {
			push(@progs, $node->{PROG});
		} elsif  (!is_duplicate($progs[-1], $node->{PROG})) {
			push(@progs, $node->{PROG});
		}

		if ($adjtree->isLeaf()) {
			push(@all_chains, join(" > ", @progs));
		} else {
			foreach $child ($adjtree->getAllChildren()) {
				get_chains($child, @progs);
			}
		}
	}
}

sub main
{
	my ($trace_file);
	
	$trace_file = $ARGV[0];

	parse_trace_file($trace_file);

	get_chains($tree);

	print "\n===============================================================================\n";
	print "====[[ Execve Relationships ]]=================================================\n\n";
	print join("\n", sort({length $a <=> length $b} uniq(@all_chains))), "\n";

	print "\n===============================================================================\n";
	print "====[[ Raw Program List ]]=====================================================\n\n";
	print join("\n", sort(uniq(@all_progs))), "\n";

	$tree->DESTROY();
}
main();
