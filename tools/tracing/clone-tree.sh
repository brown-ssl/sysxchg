#!/bin/bash

# import cookie-cutter functions
ROOT_DIR="$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd)/../.."
source "$(dirname "${0}")"/../common.sh

export AWKPATH="${TRACING_DIR}"

NONROOT_PROGS=""
ROOT_SUMMARY=""
DESC_SUMMARY=""
ROOT_SUMMARY_LATEX=""
DESC_SUMMARY_LATEX=""

do_syscall_analysis_summary()
{
    local root_prog="$1"
    local desc_progs="$2"
    local isroot="${3:-"no"}"

    comm --total --nocheck-order --output-delimiter=',' "$desc_progs" "$root_prog" | \
        awk -F[,] '@include "syscall-map.awk"
                   BEGIN { yellow = "\033[33m"; green = "\033[32m"; red = "\033[31m"; clear = "\033[0m"
                        print green "Descendant" clear "\t" red "Root" clear "\t" yellow "Shared" clear "\tName"}
                   !/total/ { str = ""
                              color = ""
                              clear = "\033[0m"
                              for (i = 1; i <= 3; i++) {
                                if (length($i) == 0) { str = str "--\t" }
                                else {if (i == 1) { color = green }
                                      else if (i == 2) { color = red }
                                      else if (i == 3) { color = yellow }
                                      num = $i; str = str color $i clear "\t" }
                              }
                              str = str color syscalls[num] clear "\n"
                              print str}
                    /total/ { print green $1 clear "\t" red $2 clear "\t" yellow $3 clear "\tTOTAL [Sum: " ($1 + $2 + $3) " (+" $1/($2 + $3) * 100.0 "%)]\n" }' | \
    column -t | sed -e '1a\---------------------------------------------------------------------------' \
                    -e '$i\---------------------------------------------------------------------------' \
                    -e '$a\---------------------------------------------------------------------------' 
    echo

    local root_syscalls="$(comm --nocheck-order -13 "$desc_progs" "$root_prog" | tr '\n' ',')"
    root_syscalls="[${root_syscalls%?}]"
    local descendant_syscalls="$(comm --nocheck-order -23 "$desc_progs" "$root_prog" | tr '\n' ',')"
    descendant_syscalls="[${descendant_syscalls%?}]"
    local shared_syscalls="$(comm --nocheck-order -12 "$desc_progs" "$root_prog" | tr '\n' ',')"
    shared_syscalls="[${shared_syscalls%?}]"

    local nroot=$(awk -F[,] '/\[\]/ { print "0" } /\[[0-9].*\]/ { print NF }' <<< $root_syscalls) 
    local ndescendant=$(awk -F[,] '/\[\]/ { print "0" } /\[[0-9].*\]/ { print NF }' <<< $descendant_syscalls) 
    local nshared=$(awk -F[,] '/\[\]/ { print "0" } /\[[0-9].*\]/ { print NF }' <<< $shared_syscalls) 

    echo -e "\033[31mRoot [$nroot syscalls]\033[0m"
    echo "Numbers:"
    echo "    $(echo "$root_syscalls" | sed 's/,/, /g')" | fmt
    echo "Names:"
    echo "    $(echo "$root_syscalls" | tr -d "[]" | \
                awk -F[,] -i ${TRACING_DIR}/syscall-map.awk -f "${TRACING_DIR}/syscall-nums-to-names.awk")" | fmt
    echo

    echo -e "\033[32mDescendant [$ndescendant syscalls]\033[0m"
    echo "Numbers:"
    echo "    $(echo "$descendant_syscalls" | sed 's/,/, /g')" | fmt
    echo "Names:"
    echo "    $(echo "$descendant_syscalls" | tr -d "[]" | \
                awk -F[,] -i ${TRACING_DIR}/syscall-map.awk -f "${TRACING_DIR}/syscall-nums-to-names.awk")" | fmt
    echo

    echo -e "\033[33mShared [$nshared syscalls]\033[0m"
    echo "Numbers:"
    echo "    $(echo "$shared_syscalls" | sed 's/,/, /g')" | fmt
    echo "Names:"
    echo "    $(echo "$shared_syscalls" | tr -d "[]" | \
                awk -F[,] -i ${TRACING_DIR}/syscall-map.awk -f "${TRACING_DIR}/syscall-nums-to-names.awk")" | fmt
    echo

    if [[ "$isroot" == "yes" ]]; then
        ROOT_SUMMARY+="\t$(($nroot + $ndescendant + $nshared)) (+$(printf '%.2f' $(calc -d $ndescendant/\($nroot + $nshared\)*100.0))%)\n"
        ROOT_SUMMARY_LATEX+="\t&\t$(($nroot + $ndescendant + $nshared))\t&\t(+$(printf '%.2f' $(calc -d $ndescendant/\($nroot + $nshared\)*100.0))\\%) \\\\\\\\\n"
    else
        DESC_SUMMARY+="\t$(($nroot + $ndescendant + $nshared)) (+$(printf '%.2f' $(calc -d $ndescendant/\($nroot + $nshared\)*100.0))%)\n"
        DESC_SUMMARY_LATEX+="\t&\t$(($nroot + $ndescendant + $nshared))\t&\t(+$(printf '%.2f' $(calc -d $ndescendant/\($nroot + $nshared\)*100.0))\\%) \\\\\\\\\n"
    fi
}

doit()
{
    echo -e "Tracing: $PROG ${ARGS[@]}\n"

    if [[ -z "$STRACE_IN" ]]; then
        [[ -z "$STRACE_OUT" ]] && STRACE_OUT=`mktemp`

        if [[ $VERBOSE == 0 ]]; then
            strace -ttt -f -n -z -e clone,fork,vfork,execve,execveat -o "$STRACE_OUT" \
                $PROG "${ARGS[@]}" > /dev/null
        else
            echo -e "\n==============================================================================="
            echo -e "====[[ Program Output ]]=======================================================\n"
            strace -ttt -f -n -z -e clone,fork,vfork,execve,execveat -o "$STRACE_OUT" \
                $PROG "${ARGS[@]}"
        fi
    else
        STRACE_OUT="$STRACE_IN"
    fi

    cat "$STRACE_OUT"

    # Deal with strace sometimes wrapping calls to the next line without marking
    # the next line as unfinished.
    awk 'NR == 1 { printf "%s", $0; next }
        /^[0-9]*\s*[0-9]*\.[0-9]*\s*\[\s*[0-9]*\].*/{printf "\n%s", $0}
        !/^[0-9]*\s*[0-9]*\.[0-9]*\s*\[\s*[0-9]*\].*/{printf "%s", $0}
        END { printf "\n" }' "$STRACE_OUT" | sort -k2,2 -g | sponge "$STRACE_OUT"

    local res=$("${TRACING_DIR}"/tree.pl "$STRACE_OUT" 2>&1)

    local progs=$(awk '
        /====/ { flag = 0 }
        flag == 1 && NF > 0 {
            if ($0 ~ /.*\<(generic-cat|generic-pp|git-revision)\>$/) {
                print "/usr/bin/sh"
                print "[Note] Using /usr/bin/sh for "$0 > "/dev/stderr"
                next
            }

            cmd1 = "file -bEL "$0
            if ((cmd1 | getline ftype) > 0) {
                if (ftype ~ /ERROR/) {
                    close(cmd1)
                    close(cmd2)
                    print "[Error] Cannot stat file: "$0 > "/dev/stderr"
                    next
                }
                if (ftype ~ /ASCII/) {
                    cmd3 = "head -n1 "$0
                    if ((cmd3 | getline interp) > 0) {
                        if (interp ~ /.*[\/]?bash.*/) {
                            print "/usr/bin/bash"
                            print "[Note] Using /usr/bin/bash for "$0 > "/dev/stderr"
                        } else if (interp ~ /.*[\/]?sh.*/) {
                            print "/usr/bin/sh"
                            print "[Note] Using /usr/bin/sh for "$0 > "/dev/stderr"
                        } else if (interp ~ /.*[\/]?node.*/) {
                            print "/usr/bin/node"
                            print "[Note] Using /usr/bin/node for "$0 > "/dev/stderr"
                        } else if (interp ~ /.*[\/]?python3.*/) {
                            print "/usr/bin/python3"
                            print "[Note] Using /usr/bin/python3 for "$0 > "/dev/stderr"
                        } else {
                            print "[Error] Unknown interpreter for "$0 > "/dev/stderr"
                        }
                    }
                    close(cmd3)
                } else {
                    print $0
                }
            }
            close(cmd1)
        }
        /====\[\[ Raw Program List \]\]=*/ { flag = 1 }' <<< "$res")

    local execves_tmp=$(awk '
        /====/ { flag = 0 }
        flag == 1 && NF > 0 {
            gsub(/[\s]?.*\<(generic-cat|generic-pp|git-revision)\>[\s]?/, " /usr/bin/sh ")

            for (i = 1; i <= NF; i++) {
                if ($i == ">") {
                    continue
                }

                cmd1 = "file -bEL "$i
                if ((cmd1 | getline ftype) > 0) {
                    if (ftype ~ /ERROR/) {
                        close(cmd1)
                        close(cmd2)
                        print "[Error] Cannot stat file: "$i > "/dev/stderr"
                        next
                    }
                    if (ftype ~ /ASCII/) {
                        cmd3 = "head -n1 "$i
                        if ((cmd3 | getline interp) > 0) {
                            if (interp ~ /.*[\/]?bash.*/) {
                                $i = "/usr/bin/bash"
                            } else if (interp ~ /.*[\/]?sh.*/) {
                                $i = "/usr/bin/sh"
                            } else if (interp ~ /.*[\/]?node.*/) {
                                $i = "/usr/bin/node"
                            } else if (interp ~ /.*[\/]?python3.*/) {
                                $i = "/usr/bin/python3"
                            } else {
                                print "[Error] Unknown interpreter for "$0 > "/dev/stderr"
                            }
                        }
                        close(cmd3)
                    }
                }
                close(cmd1)
            }

            print $0
        }
        /====\[\[ Execve Relationships \]\]=*/ { flag = 1 }' <<< "$res")

    # Deduplicate adjacent results
    local execves=""
    while IFS= read -r line; do
        execves+="
$(echo "$line" | awk -v RS='\\s>\\s' '$1 != D {printf "%s", $1 (RT?RT:ORS); D=$1}')" 
    done <<< "$execves_tmp"

    # sort/uniq results so we don't do extra work
    execves="$(echo "$execves" | sort | uniq)"

    # Print general results
    echo "$res"

    # Print sysfilter results
    echo -e "\n==============================================================================="
    echo -e "====[[ Syscall Policy Results ]]=================================================\n"
    for p in $(echo $progs | tr ' ' '\n' | sort | uniq); do
        local syscalls_json="${POLICY_DIR}/$(basename "$p").json"
        local num_syscalls="$(awk -F[,] '{ print NF }' "$syscalls_json")"
        echo "$p [$num_syscalls syscalls]:"
        echo "Numbers:"
        echo "    $(cat "$syscalls_json" | sed 's/,/, /g')" | fmt
        echo "Names:"
        echo "    $(cat "$syscalls_json" | tr -d "[]" | \
                    awk -F[,] -i ${TRACING_DIR}/syscall-map.awk -f "${TRACING_DIR}/syscall-nums-to-names.awk")" | fmt
        echo
    done

    echo -e "\n==============================================================================="
    echo -e "====[[ Syscall Set Analysis: Descendant Contributions ]]=======================\n"

    local all_syscall_sets=`mktemp`
    while IFS= read -r line; do
        local chain=($(echo "$line" | tr -s '>' ' '))

        #
        # Note: you can set i = 0 below to start the descendant analysis at the
        #       root of the tree
        #

        for ((i = 1; i < ${#chain[@]}; i++)); do
            local cur=`mktemp`
            local acc=`mktemp`

            local syscalls_cur="${POLICY_DIR}/$(basename "${chain[$i]}").json"
            cat "$syscalls_cur" | tr -d '[]' | tr ',' '\n' | sort -n | uniq > "$cur"

            # accumulate all syscall sets from program that aren't the root
            if (($i > 0)); then
                cat "$cur" >> "$all_syscall_sets"

                sort -n "$all_syscall_sets" | uniq | sponge "$all_syscall_sets"
                NONROOT_PROGS+="$(basename ${chain[$i]})\n"
            fi

            if (($i == ${#chain[@]} - 1)); then
                continue
            fi 

            for ((j = $i + 1; j < ${#chain[@]}; j++)); do
                local syscalls_desc="${POLICY_DIR}/$(basename "${chain[$j]}").json"
                cat "$syscalls_desc" | tr -d '[]' | tr ',' '\n' >> "$acc"
                sort -n "$acc" | uniq | sponge "$acc"
            done

            echo "${chain[$i]} > ($(echo "${chain[@]:$i+1}" | sed 's/ / > /g')) [depth: $(($i+1))]"
            DESC_SUMMARY+="$(echo -e "$(($i+1))\t$(basename ${chain[$i]})\t[$(echo -e "${chain[@]:$i+1}" | xargs -n1 basename | sort | uniq | tr '\n' ',' | sed 's/^,//' | sed 's/,$//')"])"
            DESC_SUMMARY_LATEX+="$(echo -e "$(($i+1))\t&\t"'\\\\'"texttt{$(basename ${chain[$i]})}\t&\t$(echo -e "${chain[@]:$i+1}" | xargs -n1 basename | xargs -n1 printf '%stexttt{%s}\n' '\\\\' | sort | uniq | perl -pe 's;\n;, ;g' | sed 's/^,//' | sed 's/, $//')")"

            do_syscall_analysis_summary "$cur" "$acc" "no"

            rm "$cur" "$acc"
        done
    done <<< "$execves"

    echo -e "\n==============================================================================="
    echo -e "====[[ Syscall Set Analysis: Root Comparison ]]================================\n"

    local root_name
    if [[ "$PROG" =~ .*\.sh$ ]]; then
        root_name="/usr/bin/bash"
    elif [[ "$PROG" =~ .*\.js$ ]]; then
        root_name="/usr/bin/node"
    else
        root_name="$PROG"
    fi

    local root=`mktemp`
    local syscalls_root="${POLICY_DIR}/$(basename "$root_name").json"
    cat "$syscalls_root" | tr -d '[]' | tr ',' '\n' | sort -n | uniq > "$root"

    echo "$root_name > (all programs)"
    ROOT_SUMMARY+="$(echo -e "1\t$(basename $root_name)\t[$(echo -e "$NONROOT_PROGS" | sort | uniq | tr '\n' ',' | sed 's/^,//' | sed 's/,$//')"])"
    ROOT_SUMMARY_LATEX+="$(echo -e "1\t&\t$(basename $root_name | xargs -n1 printf '%stexttt{%s}' '\\\\')\t&\t$(echo -e "$NONROOT_PROGS" | xargs -n1 printf '%stexttt{%s}\n' '\\\\' | sort | uniq | perl -pe 's;\n;, ;g' | sed 's/^,//' | sed 's/, $//')")"

    do_syscall_analysis_summary "$root" "$all_syscall_sets" "yes"
    rm "$root" "$all_syscall_sets"

    echo -e "\n==============================================================================="
    echo -e "====[[ Syscall Set Analysis: Table Summary ]]==================================\n"

    echo -en "Depth\tRoot\tDescendants\tTotalSyscalls\tPercentInc\n$ROOT_SUMMARY\n$DESC_SUMMARY" | column -t

    echo -e "\n==============================================================================="
    echo -e "====[[ Syscall Set Analysis: Table Summary (LaTeX) ]]==========================\n"

    echo -en "$ROOT_SUMMARY_LATEX\n$DESC_SUMMARY_LATEX"

    if [[ -z "$STRACE_IN" ]]; then
        if [[ $SAVE_STRACE == 1 ]]; then
            echo -e "\n(Saved strace output to: $STRACE_OUT)"
        else
            rm "$STRACE_OUT"
        fi
    fi
}

usage()
{
    cat 1>&2 <<EOF
Usage: $0 [Option...] -- prog [args...]

Note:
  End option list with '--' to begin positional arguments ('prog' and 'args').

Options:
  -h, --help            prints this usage message
  -v, --verbose         verbose output
  --save-strace         save strace output (default: OFF)
  --strace-out file     file to save strace output to (default: tmp file)
  --strace-in file      strace file to use (default: run strace)
  --rawprogs            print all programs executed (default: ON)
  --relationships       print execve relationships (which program execve'd
                        which other program) (default: ON)
  --tree                display results in a tree (default: ON)
  --compact             display results compactly (default: OFF)
EOF
}

main()
{
    if [[ $# -eq 0 ]]; then
        usage
        exit
    fi

    VERBOSE=0
    RAWPROGS=1
    RELATIONSHIPS=1
    TREE=1
    COMPACT=0
    SAVE_STRACE=0
    STRACE_OUT=''
    STRACE_IN=''
    while [[ $# -gt 0 ]]; do
        case "$1" in
            '-h' | '--help'     ) usage; exit ;;
            '-v' | '--verbose'  ) VERBOSE=1; shift ;;
            '--save-strace'     ) SAVE_STRACE=1; shift ;;
            '--strace-out'      ) STRACE_OUT="$2"; shift; shift ;;
            '--strace-in'       ) STRACE_IN="$2"; shift; shift ;;
            '--rawprogs'        ) RAWPROGS=1; shift ;;
            '--no-rawprogs'     ) RAWPROGS=0; shift ;;
            '--relationships'   ) RELATIONSHIPS=1; shift ;;
            '--no-relationships') RELATIONSHIPS=0; shift ;;
            '--tree'            ) TREE=1; shift ;;
            '--no-tree'         ) TREE=0; shift ;;
            '--compact'         ) COMPACT=1; shift ;;
            '--no-compact'      ) COMPACT=0; shift ;;
            '--'    ) shift; break ;;
            *       ) echo -e "Unrecognized option: $1. Assuming it is the " \
                        "program name.\n  (Please use '--' to explicitly " \
                        "specify positional arguments.)" 1>&2; break ;;
        esac
    done
    PROG="$1"
    shift
    # Small hack to deal with cases like bash -c 'some | commands'
    if [[ ("$PROG" == "bash" || \
            "$PROG" == "/bin/bash" || \
            "$PROG" == "/usr/bin/bash") && "$1" == "-c" ]]; then
        PROG="$PROG -c"
        shift
    fi
    ARGS="$@"

    doit
}
main $@
