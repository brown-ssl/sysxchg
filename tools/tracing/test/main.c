#include <unistd.h>

int main(void)
{
	if (fork() != 0) {
		char *args[] = {"file", "/bin/ls", NULL};
		execvp(args[0], args);
	}
}
