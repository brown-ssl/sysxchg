#!/usr/bin/gawk -f

###@include "./syscall-map.awk"
BEGIN {
  printf "["
}

{
  for (i = 1; i <= NF; i++) {
    printf "%d:%s", $i, syscalls[$i]

    if (i != NF)
      printf ", "
  }
}

END {
  printf "]\n"
}
