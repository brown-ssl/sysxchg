#!/bin/bash

# directory paths
ROOT_DIR=${ROOT_DIR:-"$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd)/.."}
export ROOT_DIR
VM_ROOT_DIR="/root"
export VM_ROOT_DIR 
BUILD_DIR="${ROOT_DIR}/build"
export BUILD_DIR
RESULTS_DIR="${ROOT_DIR}/results"
export RESULTS_DIR
CONFIG_DIR="${ROOT_DIR}/configs"
export CONFIG_DIR
KEYS_BUILD_DIR="${BUILD_DIR}/keys"
export KEYS_BUILD_DIR
ROOTFS_BUILD_DIR="${BUILD_DIR}/rootfs"
export ROOTFS_BUILD_DIR
ENFORCEMENT_DIR="${ROOT_DIR}/tools/enforcement"
export ENFORCEMENT_DIR
BMK_SRC_DIR="${ROOT_DIR}/bmk-src"
export BMK_SRC_DIR
POLICY_DIR="${ROOT_DIR}/policies"
export POLICY_DIR
TOOL_DIR="${ROOT_DIR}/tools"
export TOOL_DIR
TEST_DIR="${ROOT_DIR}/tests"
export TEST_DIR
MOUNT_DIR="/mnt/chroot"
export MOUNT_DIR
TRACING_DIR="${TOOL_DIR}/tracing"
export TRACING_DIR
QEMU_PIDFILE=${BUILD_DIR}/qemu_pid

# kernel/rootfs/vm stuff
KERNEL_VERSION='linux-6.0.8'
export KERNEL_VERSION
KERNEL_SRC_DIR="${ROOT_DIR}/${KERNEL_VERSION}"
export KERNEL_SRC_DIR
ROOTFS_IMG="${ROOTFS_BUILD_DIR}/bullseye.img"
export ROOTFS_IMG
KERNEL_CONFIGS="vanilla inheritance exchange"
export KERNEL_CONFIGS
ALL_KERNEL_CONFIGS="vanilla vanilla-dbg \
                     inheritance inheritance-dbg \
                     exchange exchange-dbg"
export KERNEL_CONFIGS
INHERITANCE_FILTER_TYPES="vanilla xfilter seccomp"
export INHERITANCE_FILTER_TYPES
BENCHMARKS="spec sqlite pash"
export BENCHMARKS
TEST_VM_SSHKEY=${ROOTFS_BUILD_DIR}/bullseye.id_rsa 
export TEST_VM_SSHKEY
TEST_VM_PUBKEY=${TEST_VM_SSHKEY}.pub
export TEST_VM_PUBKEY
VMPORT=6969

# IMA/EVM stuff
IMA_GROUP="ima"
export IMA_GROUP
IMA_KEY="/etc/keys/privkey_ima.pem"
export IMA_KEY
EVM_KEY="/etc/keys/privkey_evm.pem"
export EVM_KEY
IMA_POLICY_TEMPLATE="appraise func=BPRM_CHECK fgroup=__GID__ appraise_type=imasig"
export IMA_POLICY_TEMPLATE
EVM_ENABLE_VALUE="0x80000002"
export EVM_ENABLE_VALUE
IMA_POLICY_FILE="/sys/kernel/security/integrity/ima/policy"
export IMA_POLICY_FILE
EVM_POLICY_FILE="/sys/kernel/security/integrity/evm/evm"
export EVM_POLICY_FILE

# scripts
ENFORCE="${ROOT_DIR}/tools/enforce.sh"
SPEC_SCRIPT="${VM_ROOT_DIR}/tools/spec.sh"
export SPEC_SCRIPT
SQLITE_SCRIPT="${VM_ROOT_DIR}/tools/sqlite.sh"
export SQLITE_SCRIPT
PASH_SCRIPT="${VM_ROOT_DIR}/tools/pash.sh"
export PASH_SCRIPT
ENABLE_IMA_SCRIPT="${VM_ROOT_DIR}/tools/enable-ima.sh"
export ENABLE_IMA_SCRIPT
BUILD_SCRIPT="${TOOL_DIR}/build.sh"
export BUILD_SCRIPT
RUN_SCRIPT="${TOOL_DIR}/run.sh"
export RUN_SCRIPT
CLEAN_SCRIPT="${TOOL_DIR}/clean.sh"
export CLEAN_SCRIPT
DEBUG_SCRIPT="${TOOL_DIR}/debug.sh"
export DEBUG_SCRIPT
VM_TEST_DIR="${VM_ROOT_DIR}/tests"
export VM_TEST_DIR
TEST_SCRIPT="./run-tests.sh"
export TEST_SCRIPT

# color codes
RED='\033[0;31m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
RESET='\033[0m'

# logging
do_log()
{
    echo -e \
        "[+][$(TZ='America/New_York' /usr/bin/date +%Y/%m/%d\ @\ %H:%M:%S)] " \
        "${GREEN}${1}${RESET}"
}

do_warn()
{
    echo -e \
        "[!][$(TZ='America/New_York' /usr/bin/date +%Y/%m/%d\ @\ %H:%M:%S)] " \
        "${ORANGE}${1}${RESET}"
}

do_error()
{
    echo -e \
        "[Error][$(TZ='America/New_York' /usr/bin/date +%Y/%m/%d\ @\ %H:%M:%S)] " \
        "${RED}${1}${RESET}" 1>&2
    exit 1
}

verify_config_opt()
{
    local config="${1}"

    for c in ${ALL_KERNEL_CONFIGS}; do
        if [[ ${config} == "${c}" ]]; then
            return
        fi
    done

    do_error "Invalid config: ${config}."
}

verify_suite_opt()
{
    local suite="${1}"

    if [[ ${suite} != "inheritance" && ${suite} != "exchange" ]]; then
        do_error "Invalid test suite: ${suite}."
    fi
}

verify_bmk_opt()
{
    local bmk="${1}"

    for b in ${BENCHMARKS}; do
        if [[ ${b} == "${bmk}" ]]; then
            return
        fi
    done

    do_error "Invalid benchmark: ${bmk}."
}

do_enforce_common()
{
    local config="${1}"
    local bins="${2}"
    local bname
    local policy

    for bin in ${bins}; do

        bname="$(basename "${bin}")"
        if [[ ${bname} == speedtest1* ]]; then
            policy="${POLICY_DIR}/speedtest1.json"
        else
            policy="${POLICY_DIR}/${bname%_base*}.json"
        fi

        if [[ "${config}" == "xfilter" ]]; then
            # check if binary is already enforced
            if readelf -S "${bin}" | grep -q ".xfilter"; then
                do_log "Binary (${bin}) already enforced, skipping..."
                continue
            fi

            ${ENFORCE} "${bin}" "${policy}" "${POLICY_DIR}"/default/*

        elif [[ "${config}" == "seccomp" ]]; then
            # check if binary is already enforced
            if readelf -S "${bin}" | grep -q ".seccomp"; then
                do_log "Binary (${bin}) already enforced, skipping..."
                continue
            fi

            ${ENFORCE} --seccomp "${bin}" "${policy}" "${POLICY_DIR}"/default/*
        fi

        do_log "Done policy embedding in ${bin}."
    done
}

setup_ima()
{
    if ! grep -q "^${IMA_GROUP}:" /etc/group; then
        addgroup --system "${IMA_GROUP}"
    fi
}

do_sign_common()
{
    local config="${1}"
    local bins="${2}"

    setup_ima

    if [[ ${config} == "vanilla" ]]; then
        return
    fi

    for bin in ${bins}; do
        # skip files that already signed
        if [[ -n "$(getfattr -m . -d "${bin}")" ]]; then 
            do_log "Binary already signed, skipping... (${bin})"
            continue
        fi

        # change file owner's group to be ima for easy policy appraisal
        chown :"${IMA_GROUP}" "${bin}"

        # sign for IMA first...
        evmctl ima_sign --key "${IMA_KEY}" "${bin}"
        # ...then EVM
        evmctl sign --key "${EVM_KEY}" "${bin}"

        do_log "Done signing ${bin}."
    done
}

do_verify_filters()
{
    local config="${1}"
    local bins="${2}"

    local all_good=1

    for bin in ${bins}; do
        if [[ "${config}" == "xfilter" ]]; then
            if ! readelf -S "${bin}" | grep -q ".xfilter"; then
                do_warn "Binary not enforced: ${bin}"
                all_good=0
            fi
        elif [[ "${config}" == "seccomp" ]]; then
            if ! readelf -S "${bin}" | grep -q ".seccomp"; then
                do_warn "Binary not enforced: ${bin}"
                all_good=0
            fi
        fi
    done

    [[ ${all_good} -eq 0 ]] && return 1

    return 0
}

do_verify_signatures()
{
    local config="${1}"
    local bins="${2}"

    local all_good=1

    for bin in ${bins}; do
				if ! getfattr -m . -d "${bin}" | grep -q "security\.evm"; then
            do_warn "Binary missing evm signature: ${bin}."
            all_good=0
        fi
				if ! getfattr -m . -d "${bin}" | grep -q "security\.ima"; then
            do_warn "Binary missing ima signature: ${bin}."
            all_good=0
        fi
        if [[ -z "$(find "${bin}" -group ima)" ]]; then
            do_warn "Binary not owned by group 'ima': ${bin}."
            all_good=0
        fi
    done

    [[ ${all_good} -eq 0 ]] && return 1

    return 0
}

vmcmd()
{
    local cmd="${1}"

    ssh -i "${TEST_VM_SSHKEY}" -p "${VMPORT}" -o UserKnownHostsFile=/dev/null \
        -o StrictHostKeyChecking=no -q root@localhost "${cmd}"
}

wait_vm()
{
    vmcmd "exit"
    while [[ ${?} -ne 0 ]]; do
        vmcmd "exit"
    done
}

vmcopy()
{
    local src="${1}"
    local dst="${2}"

    scp -r -i "${TEST_VM_SSHKEY}" -P "${VMPORT}" -o UserKnownHostsFile=/dev/null \
        -o StrictHostKeyChecking=no -q root@localhost:"${src}" "${dst}"
}

vmstart()
{
    local config="${1:-vanilla}"
    shift

    local kernel_image="${BUILD_DIR}/${KERNEL_VERSION}-${config}/arch/x86/boot/bzImage"
    local kernel_flags="root=/dev/sda rw console=ttyS0 quiet nokaslr net.ifnames=0"
    local dbg=0
    local snapshot=""

    do_log "Starting ${config} VM."

    local ima_already_set=0
    while [[ ${#} -gt 0 ]]; do
        case "${1}" in
            ima_measure )
                if [[ ${ima_already_set} -eq 0 ]]; then
                    kernel_flags+=" ima_appraise=fix evm=fix"
                else
                    do_warn "Ignoring kernel option. IMA flags already set..."
                fi
                ima_already_set=1
                ;;

            ima_appraise )
                if [[ ${ima_already_set} -eq 0 ]]; then
                    kernel_flags+=" ima_appraise=enforce"
                else
                    do_warn "Ignoring kernel option. IMA flags already set..."
                fi
                ima_already_set=1
                ;;

            debug )
                dbg=1
                ;;

            snapshot )
                snapshot="-snapshot"
                ;;

            * )
                do_error "Invalid argument: ${1}."
                ;;
        esac
        shift
    done

    if [[ ${dbg} -eq 1 ]]; then
        # start me up
        qemu-system-x86_64 \
          -kernel "${kernel_image}" \
          -drive file="${ROOTFS_IMG}",index=0,media=disk,format=raw \
          -nographic \
          -append "${kernel_flags}" \
          -m 8192 \
          -smp 4 \
          --enable-kvm \
          -device e1000,netdev=net0 \
          -netdev user,id=net0,hostfwd=tcp:127.0.0.1:${VMPORT}-:22 \
          -cpu host \
          -s \
          ${snapshot}
    else
        # start me up
        qemu-system-x86_64 \
          -kernel "${kernel_image}" \
          -drive file="${ROOTFS_IMG}",index=0,media=disk,format=raw \
          -nographic \
          -append "${kernel_flags}" \
          -m 8192 \
          -smp 4 \
          --enable-kvm \
          -device e1000,netdev=net0 \
          -netdev user,id=net0,hostfwd=tcp:127.0.0.1:${VMPORT}-:22 \
          -cpu host \
          -serial null\
          -pidfile ${QEMU_PIDFILE} \
          ${snapshot} > /dev/null &

        wait_vm
    fi
}

vmstop()
{
    vmcmd "shutdown -h now"

    # easier to just sleep for a few seconds instead of actually checking
    # if things shutdown correctly
    wait $(cat ${QEMU_PIDFILE})

    do_log "Stopped VM."
}
