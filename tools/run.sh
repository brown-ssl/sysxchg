#!/bin/bash

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

run_test()
{
    local suite="${1}"

    case "${suite}" in

        inheritance )
            vmstart inheritance snapshot
            vmcmd "cd ${VM_TEST_DIR} && ${TEST_SCRIPT} inheritance"
            vmstop
            ;;

        exchange )
            vmstart exchange ima_appraise snapshot
            vmcmd "${ENABLE_IMA_SCRIPT}"
            vmcmd "cd ${VM_TEST_DIR} && ${TEST_SCRIPT} exchange"
            vmstop
            ;;

        * )
            do_error "Invalid test suite: ${suite}."
            ;;
    esac
}

run_performance()
{
    local bmk="${1}"

    mkdir -p "${RESULTS_DIR}"

    case "${bmk}" in
    
        spec )
            # get SPEC baseline results
            vmstart vanilla snapshot
            vmcmd "${SPEC_SCRIPT} --filter vanilla --benchmark"
            vmcopy "/tmp/cpu2017-intspeed-vanilla.log" "${RESULTS_DIR}"
            vmcopy "/tmp/cpu2017-fpspeed-vanilla.log" "${RESULTS_DIR}"
            vmstop

            # get the SPEC results for xfilter and seccomp filtering
            vmstart inheritance snapshot
            vmcmd "${SPEC_SCRIPT} --filter xfilter --benchmark"
            vmcopy "/tmp/cpu2017-intspeed-xfilter.log" "${RESULTS_DIR}"
            vmcopy "/tmp/cpu2017-fpspeed-xfilter.log" "${RESULTS_DIR}"
            vmcmd "${SPEC_SCRIPT} --filter seccomp --benchmark"
            vmcopy "/tmp/cpu2017-intspeed-seccomp.log" "${RESULTS_DIR}"
            vmcopy "/tmp/cpu2017-fpspeed-seccomp.log" "${RESULTS_DIR}"
            vmstop
            ;;

        sqlite )
            # get SQLite baseline results
            vmstart vanilla snapshot
            vmcmd "${SQLITE_SCRIPT} --filter vanilla --benchmark"
            vmcopy "/tmp/speedtest1-vanilla.log" "${RESULTS_DIR}"
            vmstop

            # get the SQLite results for xfilter and seccomp filtering
            vmstart inheritance snapshot
            vmcmd "${SQLITE_SCRIPT} --filter xfilter --benchmark"
            vmcopy "/tmp/speedtest1-xfilter.log" "${RESULTS_DIR}"
            vmcmd "${SQLITE_SCRIPT} --filter seccomp --benchmark"
            vmcopy "/tmp/speedtest1-seccomp.log" "${RESULTS_DIR}"
            vmstop
            ;;

        pash )
            # get PaSH baseline results
            vmstart vanilla snapshot
            vmcmd "${PASH_SCRIPT} --filter vanilla --benchmark"
            vmcopy "/tmp/pash-vanilla" "${RESULTS_DIR}"
            vmstop

            # get the PaSH results for xfilter and seccomp filtering
            vmstart exchange snapshot ima_appraise
            vmcmd "${ENABLE_IMA_SCRIPT}"
            vmcmd "${PASH_SCRIPT} --filter xfilter --benchmark"
            vmcopy "/tmp/pash-xfilter" "${RESULTS_DIR}"
            vmcmd "${PASH_SCRIPT} --filter seccomp --benchmark"
            vmcopy "/tmp/pash-seccomp" "${RESULTS_DIR}"
            vmstop
            ;;

        * )
            do_error "Invalid benchmark: ${bmk}."
            ;;
    esac
}

run_effectiveness()
{
    vmstart vanilla snapshot
    vmcmd "${PASH_SCRIPT} --effectiveness"
    vmcopy "/tmp/effectiveness" "${RESULTS_DIR}"
    vmstop
}

print_usage()
{
    cat 1>&2 <<EOF
Usage: $(ps -o args= ${PPID} | cut -d' ' -f2) run [options]

Options:
  help                  print this help menu
  tests <suite>         test the functionality of test suite <suite>
  performance <bmk>     test the performance with benchmark <bmk>
  effectiveness         analyze capabilities differences

Test suites <suite>:
  inheritance           test suites under inheritance model
  exchange              test suites under exchange model

Performance benchmarks <bmk>:
  spec                  SPEC CPU 2017
  sqlite                SQLite Speedtest
  pash                  a subset of PaSH benchmarks
EOF
}

main()
{
    # check for command line arguments
    if [[ ${#} == 0 ]]; then
        print_usage
        exit 1
    fi

    case "${1}" in
        tests )
            suite="${2}"
            verify_suite_opt "${suite}"
            run_test "${suite}"
            ;;

        performance )
            bmk="${2}"
            verify_bmk_opt "${bmk}"
            run_performance "${bmk}"
            ;;

        effectiveness )
            run_effectiveness
            ;;

        help )
            print_usage
            ;;

        * )
            do_error "Invalid argument: ${1}."
            ;;
    esac
}
main ${@}
