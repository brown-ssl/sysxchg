#!/bin/bash

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

SQLITE_SRC="sqlite-src-3340100"
SQLITE_ZIP="${BMK_SRC_DIR}/${SQLITE_SRC}.zip"
SQLITE_SRC_DIR="${BUILD_DIR}/sqlite"

# SQLite benchmark name and binary name
BIN="speedtest1"

# user-provided arguments
FILTER_TYPE="vanilla"
ACTION=""
ITERS=10

do_install()
{
    # setup source
    unzip "${SQLITE_ZIP}" -d "${BMK_SRC_DIR}"
    mv "${BMK_SRC_DIR}"/"${SQLITE_SRC}" "${SQLITE_SRC_DIR}"
    cd "${SQLITE_SRC_DIR}"

    do_log "Unpacked SQLite."
}

do_build()
{
    cd "${SQLITE_SRC_DIR}"

    ./configure
    make ${BIN}
    for type in ${INHERITANCE_FILTER_TYPES}; do
        cp ${BIN} ${BIN}-${type}
    done

    do_log "Done building SQLite benchmark."
}

do_benchmark()
{
    local log="/tmp/speedtest1-${FILTER_TYPE}.log"

    cd "${SQLITE_SRC_DIR}"

    for i in $(seq ${ITERS}); do
        do_log "[${i}/${ITERS}] Starting benchmark run for speedtest1."
        ./speedtest1-${FILTER_TYPE} --memdb >> "${log}"
    done

    do_log "Done running ${FILTER_TYPE} benchmarks for SQLite" \
        "(iters=${ITERS})."
}

do_clean()
{
    read -p "Are you sure you want to clean SQLite? " -n 1 -r
    echo
    if [[ ${REPLY} =~ ^[Yy]$ ]]; then
        cd "${SQLITE_SRC_DIR}"

        make clean
        for type in ${INHERITANCE_FILTER_TYPES}; do
            rm ${BIN}-${type}
        done

        do_log "Done cleaning up SQLite."
    fi
}

do_enforce()
{
    cd "${SQLITE_SRC_DIR}"

    for type in ${INHERITANCE_FILTER_TYPES}; do
        do_enforce_common "${type}" "${BIN}-${type}"
    done

    do_log "Done enforcing SQLite."
}

do_verify()
{
    cd "${SQLITE_SRC_DIR}"

    for type in ${INHERITANCE_FILTER_TYPES}; do
        if do_verify_filters "${type}" "${BIN}-${type}"; then
            do_log "Verification of SQLite binary ${BIN}-${type} successful!"
        else
            do_warn "Verification of SQLite binary ${BIN}-${type} unsuccessful!"
        fi
    done
}

print_usage()
{
    cat 1>&2 <<EOF
Usage: ${0} [options]

Options:
  --install             install/unpack SQLite 
  --filter <type>       specify filter type (default: vanilla)
  --build               build benchmarks for a specific filter type
  --enforce             enforce benchmarks with a specific type of filter
  --verify              check benchmarks are enforced with a filter type
  --benchmark           run benchmarks of a specific filter type
  --iters N             benchmark iterations (default: 5)
  --clean               clean up for a given filter type
  -h, --help            print this help menu

Filter types:
  vanilla   no enforcement (default)
  xfilter   syscall filtering with xfilter
  seccomp   syscall filtering with seccomp
EOF
}

main()
{
    # check for command line arguments
    if [[ ${#} == 0 ]]; then
        print_usage
        exit 1
    fi

    # parse command-line arguments
    while [[ ${#} -gt 0 ]]; do

        case "${1}" in
            --install   ) ACTION="do_install"   ;;
            --filter    ) FILTER_TYPE="${2}"; shift  ;;
            --build     ) ACTION="do_build"     ;;
            --enforce   ) ACTION="do_enforce"   ;;
            --verify    ) ACTION="do_verify"    ;;
            --benchmark ) ACTION="do_benchmark" ;;
            --iters     ) ITERS="${2}"; shift   ;;
            --clean     ) ACTION="do_clean"     ;;
            -h | --help ) ACTION="print_usage"  ;;
            *           ) do_error "Invalid argument: ${1}." ;;
        esac
        shift

    done

    # argument validation
    [[ -z ${ACTION} ]] && do_error "No action provided."

    [[ ${FILTER_TYPE} != "vanilla"
        && ${FILTER_TYPE} != "xfilter"
        && ${FILTER_TYPE} != "seccomp" ]] && do_error "Invalid filter type."

    [[ ${ACTION} == "do_benchmark"
        && ${ITERS} -lt 1 ]] && do_error "Invalid size."

    # do the action
    ${ACTION}
}
main ${@}
