#!/bin/bash

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

enable_ima()
{
    local ima_gid
    local policy

    ima_gid="$(getent group ${IMA_GROUP} | cut -d: -f3)"
    policy="${IMA_POLICY_TEMPLATE//__GID__/${ima_gid}}"

    echo "${policy}" > "${IMA_POLICY_FILE}"
		echo ${EVM_ENABLE_VALUE} > "${EVM_POLICY_FILE}"

		do_log "IMA/EVM enabled."
}

verify_ima_enabled()
{
		local dummy_ls="/tmp/ls"

		# create a dummy ls
    cp "$(which ls)" "${dummy_ls}"

		# make it part of the ima group
		chown :"${IMA_GROUP}" "${dummy_ls}"

		# try to run it without a signature (this shouldn't work)
		if ! ${dummy_ls} &> /dev/null; then
			do_log "IMA/EVM is indeed enabled."
		else
			do_warn "IMA/EVM may not be enabled or working properly."
		fi
}

main()
{
    enable_ima
    verify_ima_enabled
}
main ${@}
