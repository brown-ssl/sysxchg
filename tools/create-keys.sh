#!/bin/bash

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

CA_GENKEY='ima-local-ca.genkey'
GENKEY='ima.genkey'

mkdir -p "${KEYS_BUILD_DIR}"
cd "${KEYS_BUILD_DIR}"

cat << __EOF__ > "${CA_GENKEY}"
 # Begining of the file
[ req ]
default_bits = 2048
distinguished_name = req_distinguished_name
prompt = no
string_mask = utf8only
x509_extensions = v3_ca

[ req_distinguished_name ]
O = IMA-CA
CN = IMA/EVM certificate signing key
emailAddress = ca@ima-ca

[ v3_ca ]
basicConstraints=CA:TRUE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always,issuer
# keyUsage = cRLSign, keyCertSign
# EOF
__EOF__

cat << __EOF__ > "${GENKEY}"
# Begining of the file
[ req ]
default_bits = 1024
distinguished_name = req_distinguished_name
prompt = no
string_mask = utf8only
x509_extensions = v3_usr

[ req_distinguished_name ]
O = `hostname`
CN = `whoami` signing key
emailAddress = `whoami`@`hostname`

[ v3_usr ]
basicConstraints=critical,CA:FALSE
#basicConstraints=CA:FALSE
keyUsage=digitalSignature
#keyUsage = nonRepudiation, digitalSignature, keyEncipherment
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid
#authorityKeyIdentifier=keyid,issuer
# EOF
__EOF__

#
# ima local ca keys
#

# generate private key and X509 public key certificate
openssl req -new -x509 -nodes -utf8 -sha1 -days 36500 -batch \
    -config "${CA_GENKEY}" -outform DER -out ima-local-ca.x509 \
    -keyout ima-local-ca.priv

# produce X509 in DER format for using while building the kernel
openssl x509 -inform DER -in ima-local-ca.x509 -out ima-local-ca.pem

#
# ima keys
#

# generate private key and X509 public key certificate signing request
openssl req -new -nodes -utf8 -sha1 -days 36500 -batch -config ${GENKEY} \
    -out csr_ima.pem -keyout privkey_ima.pem

# sign X509 public key certificate signing request with local IMA CA private key
openssl x509 -req -in csr_ima.pem -days 36500 -extfile ${GENKEY} \
     -extensions v3_usr -CA ima-local-ca.pem -CAkey ima-local-ca.priv \
    -CAcreateserial -outform DER -out x509_ima.der

#
# evm keys
#

# generate private key and X509 public key certificate signing request
openssl req -new -nodes -utf8 -sha1 -days 36500 -batch -config ${GENKEY} \
    -out csr_evm.pem -keyout privkey_evm.pem

# sign X509 public key certificate signing request with local IMA CA private key
openssl x509 -req -in csr_evm.pem -days 36500 -extfile ${GENKEY} \
     -extensions v3_usr -CA ima-local-ca.pem -CAkey ima-local-ca.priv \
    -CAcreateserial -outform DER -out x509_evm.der
