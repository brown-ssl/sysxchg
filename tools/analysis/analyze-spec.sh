#!/bin/bash

ROOT_DIR=${ROOT_DIR:-"$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd)/../.."}
RESULTS_DIR="${ROOT_DIR}/results"
TYPES="vanilla xfilter seccomp"
INTSPEED_BMKS=" \
600.perlbench_s \
602.gcc_s \
605.mcf_s \
620.omnetpp_s \
623.xalancbmk_s \
625.x264_s \
631.deepsjeng_s \
641.leela_s \
657.xz_s"
FPSPEED_BMKS=" \
619.lbm_s \
638.imagick_s \
644.nab_s"

print_between()
{
  local start="${1}"
  local end="${2}"
  local file="${3}"

  start="$(sed 's;\\;\\\\;g' <<< "${start}")"
  end="$(sed 's;\\;\\\\;g' <<< "${end}")"

  awk -v start="${start}" -v end="${end}" '
    ($0 ~ end) { flag = 0 }
    (flag == 1) { print $0 }
    ($0 ~ start) { flag = 1 }' "${file}"
}

printf "\n(Displayed results are average runtime overhead in seconds.)\n\n"
printf "====================================================================\n"
printf "%-20s %15s %15s %15s\n" "Benchmark" "Vanilla" "xfilter" "Seccomp-BPF"
printf -- "--------------------------------------------------------------------\n"
for bmk in ${INTSPEED_BMKS}; do
    printf "%-20s" "${bmk}"
    for type in ${TYPES}; do
        file="${RESULTS_DIR}/cpu2017-intspeed-${type}.log"
        avg="$(print_between "Benchmarks" "===========" "${file}" | \
                awk -v bmk="${bmk}" \
                '($0 ~ bmk){s+=$3; c+=1} END {printf "%.2f\n", s/c}')"
        printf " %15s" "${avg}"
    done
    printf "\n"
done
for bmk in ${FPSPEED_BMKS}; do
    printf "%-20s" "${bmk}"
    for type in ${TYPES}; do
        file="${RESULTS_DIR}/cpu2017-fpspeed-${type}.log"
        avg="$(print_between "Benchmarks" "========" "${file}" | \
                awk -v bmk="${bmk}" \
                '($0 ~ bmk){s += $3; c += 1} END {printf "%.2f\n", s/c}')"
        printf " %15s" "${avg}"
    done
    printf "\n"
done
printf "====================================================================\n"
