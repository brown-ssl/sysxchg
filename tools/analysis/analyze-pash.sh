#!/bin/bash

ROOT_DIR=${ROOT_DIR:-"$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd)/../.."}
RESULTS_DIR="${ROOT_DIR}/results"
TYPES="vanilla xfilter seccomp"
LOG_FILES=" \
bi-grams.log \
compression.log \
diff.log \
encryption.log \
nfa-regex.log \
nlp.log \
noaa.log \
pcap.log \
set-diff.log \
shortest-scripts.log \
sort.log \
sort-sort.log \
spell.log \
top-n.log \
wf.log"

printf "\n(Displayed results are average runtime overhead in seconds.)\n\n"
printf "====================================================================\n"
printf "%-20s %15s %15s %15s\n" "Benchmark" "Vanilla" "xfilter" "Seccomp-BPF"
printf -- "--------------------------------------------------------------------\n"
for log in ${LOG_FILES}; do
    printf "%-20s" "${log%.log}"
    for type in ${TYPES}; do
        file="${RESULTS_DIR}/pash-${type}/${log}"
        avg="$(awk '{s+=$2} END {printf "%.2f\n", s/NR}' "${file}")"
        printf " %15s" "${avg}"
    done
    printf "\n"
done
printf "====================================================================\n"
