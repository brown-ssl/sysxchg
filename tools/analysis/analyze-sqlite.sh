#!/bin/bash

ROOT_DIR=${ROOT_DIR:-"$(CDPATH='' cd -- "$(dirname -- "$0")" && pwd)/../.."}
RESULTS_DIR="${ROOT_DIR}/results"
TYPES="vanilla xfilter seccomp"

for type in ${TYPES}; do
    results="$(cat "${RESULTS_DIR}/speedtest1-${type}.log" | awk '/TOTAL/ {print $2}' | sed 's/s//')"
    echo "${results}" | awk -v t="${type}" '{s+=$1} END {printf "%s: %f\n", t, s/NR}' RS=" "
done
