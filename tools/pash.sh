#!/bin/bash

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

PASH_TARXZ="${BMK_SRC_DIR}/pash.tar.xz"
PASH_SRC_DIR="${BUILD_DIR}/pash"
PASH_BIN_DIR="${PASH_SRC_DIR}/bins"

ONELINER_DIR="${PASH_SRC_DIR}/oneliners"

TIMING_DIR="${TOOL_DIR}/timing"
DO_TIME="${TIMING_DIR}/do_time"

# One liner benchmark collection
ONELINER_BMKS=" \
bi-grams.sh \
diff.sh \
nfa-regex.sh \
set-diff.sh \
shortest-scripts.sh \
sort.sh \
sort-sort.sh \
spell.sh \
top-n.sh \
wf.sh"

BMKS=" \
nlp.sh \
noaa.sh \
pcap.sh \
encryption.sh \
compression.sh"

ONELINER_BINS=" \
bash \
cat \
col \
comm \
cut \
diff \
file \
grep \
head \
iconv \
mkfifo \
mktemp \
paste \
rm \
sed \
sort \
tail \
tee \
tr \
uniq \
wc \
xargs"

BINS=" \
awk \
basename \
bash \
cat \
curl \
cut \
grep \
gzip \
head \
ls \
mkdir \
openssl \
paste \
rev \
rm \
sed \
seq \
sh \
sort \
tail \
tcpdump \
tr \
uniq \
xargs \
zip"

SCRIPTS="egrep gunzip"

PASH_BINS="$(echo -e "${ONELINER_BINS} ${BINS}" | tr " " "\n" | sort | uniq)"
ALL_PROGS="${PASH_BINS} ${SCRIPTS}"

# user-provided arguments
FILTER_TYPE="vanilla"
ACTION=""
ITERS=10

do_install()
{
    # setup source
    cp -r "${BMK_SRC_DIR}/pash" "${PASH_SRC_DIR}"

    # copy bins for enforcement of vanilla
    mkdir -p "${PASH_BIN_DIR}/vanilla"
    for bin in ${ALL_PROGS}; do
        cp -Ln "$(which ${bin})" "${PASH_BIN_DIR}/vanilla/"
    done

    # make sure scripts use the proper shell
    sed -i 's;#!/bin/sh;#!/usr/bin/env sh;' "${PASH_BIN_DIR}/vanilla/egrep"
    sed -i 's;#!/bin/sh;#!/usr/bin/env sh;' "${PASH_BIN_DIR}/vanilla/gunzip"

    # copy vanilla setup for xfilter and seccomp
    cp -rLn "${PASH_BIN_DIR}/vanilla" "${PASH_BIN_DIR}/xfilter"
    cp -rLn "${PASH_BIN_DIR}/vanilla" "${PASH_BIN_DIR}/seccomp"

    # make timing binary
    cd "${TIMING_DIR}"
    make

    do_log "Unpacked and setup PaSH."
}

do_benchmark_oneliners()
{
    local saved_path
    local log

    cd "${ONELINER_DIR}"

    # setup env
    export PASH_TOP="${PASH_SRC_DIR}"
    unset IN dict
    export IN="${PASH_TOP}/oneliners/input/1M.txt"
    export dict="${PASH_TOP}/oneliners/input/dict.txt"

    mkdir -p "/tmp/pash-${FILTER_TYPE}"

    # save a set PATH
    saved_path="${PATH}"
    export PATH="${PASH_BIN_DIR}/${FILTER_TYPE}"

    for bmk in ${ONELINER_BMKS}; do
        log="/tmp/pash-${FILTER_TYPE}/${bmk%.sh}.log"

        do_log "Starting benchmark: ${bmk%.sh} [Enforcement: ${FILTER_TYPE}]"
        for (( i = 0; i < ITERS; i++ )); do
            # Use `time` bash keyword which uses gettimeofday for the real
            # time and getrusage for the user/system times
            { 
                "${DO_TIME}" "${PASH_BIN_DIR}"/"${FILTER_TYPE}"/bash -c ./"${bmk}" >/dev/null
            } 2>>"${log}"
        done
    done

    # restore path
    export PATH="${saved_path}"
}

do_benchmark_xxx()
{
    local saved_path
    local log

    # setup env
    export PASH_TOP="${PASH_SRC_DIR}"

    mkdir -p "/tmp/pash-${FILTER_TYPE}"

    # save a set PATH
    saved_path="${PATH}"
    export PATH="${PASH_BIN_DIR}/${FILTER_TYPE}"

    for bmk in ${BMKS}; do
        cd "${PASH_SRC_DIR}/${bmk%.sh}"
        log="/tmp/pash-${FILTER_TYPE}/${bmk%.sh}.log"

        # who knows what might have set these; unset them!
        unset IN OUT ENTRIES

        do_log "Starting benchmark: ${bmk%.sh} [Enforcement: ${FILTER_TYPE}]"
        for (( i = 0; i < ITERS; i++ )); do
            { 
                "${DO_TIME}" "${PASH_BIN_DIR}"/"${FILTER_TYPE}"/bash -c ./"${bmk}" >/dev/null
            } 2>>"${log}"
            #/usr/bin/sudo /usr/bin/dmesg -c > "$log_dir/$ENFORCE_TYPE-$bmk_name-$i.dmesg.log"
        done
    done

    # restore path
    export PATH="${saved_path}"
}

do_benchmark()
{
    do_benchmark_oneliners
    do_benchmark_xxx

    do_log "Done running ${FILTER_TYPE} benchmarks for PaSH" \
        "(iters=${ITERS})."
}

do_clean()
{
    read -p "Are you sure you want to clean PaSH? " -n 1 -r
    echo
    if [[ ${REPLY} =~ ^[Yy]$ ]]; then
        rm -rf "${PASH_SRC_DIR}"
        do_log "Done cleaning up PaSH."
    fi
}

do_enforce()
{
    cd "${PASH_BIN_DIR}/${FILTER_TYPE}"
    for bin in ${PASH_BINS}; do
        do_enforce_common "${FILTER_TYPE}" "${bin}"
        do_sign_common "${FILTER_TYPE}" "${bin}"
    done

    do_log "Done enforcing PaSH for ${FILTER_TYPE} filter type."
}

do_verify()
{
    cd "${PASH_BIN_DIR}/${FILTER_TYPE}"
    for bin in ${PASH_BINS}; do
        if do_verify_filters "${FILTER_TYPE}" "${bin}"; then
            if do_verify_signatures "${FILTER_TYPE}" "${bin}"; then
                do_log "Verification of PaSH binary ${FILTER_TYPE} ${bin} " \
                        "successful!"
            else
                do_warn "Verification of PaSH binary ${FILTER_TYPE} ${bin} " \
                        "unsuccessful (no signature)!"
            fi
        else
            do_warn "Verification of PaSH binary ${FILTER_TYPE} ${bin} " \
                    "unsuccessful (no filter)!"
        fi
    done
}

do_effectiveness()
{
    # make sure Tree::Simple is installed for perl
    cpanm Tree::Simple

    # setup env
    export PASH_TOP="${PASH_SRC_DIR}"
    mkdir -p "/tmp/effectiveness/oneliners"

    # oneliners env var setup
    unset IN dict
    export IN="${PASH_TOP}/oneliners/input/1M.txt"
    export dict="${PASH_TOP}/oneliners/input/dict.txt"

    # oneliners
    cd "${PASH_SRC_DIR}/oneliners"
    for bmk in ${ONELINER_BMKS}; do
        log="/tmp/effectiveness/oneliners/${bmk%.sh}.log"
        { "${TRACING_DIR}"/clone-tree.sh -- ./"${bmk}"; } > "${log}"
    done

    # other benchmarks
    for bmk in ${BMKS}; do
        cd "${PASH_SRC_DIR}/${bmk%.sh}"
        log="/tmp/effectiveness/${bmk%.sh}.log"

        # who knows what might have set these; unset them!
        unset IN OUT ENTRIES

        do_log "Tracing: ${bmk%.sh}."
        { "${TRACING_DIR}"/clone-tree.sh -- ./"${bmk}"; } > "${log}"
    done
}

print_usage()
{
    cat 1>&2 <<EOF
Usage: ${0} [options]

Options:
  --install             install/unpack PaSH
  --filter <type>       specify filter type (default: vanilla)
  --enforce             enforce benchmarks with a specific type of filter
  --verify              check benchmarks are enforced with a filter type
  --benchmark           run benchmarks of a specific filter type
  --iters N             benchmark iterations (default: 5)
  --effectiveness       run effectiveness tests
  --clean               clean up for a given filter type
  -h, --help            print this help menu

Filter types:
  vanilla   no enforcement (default)
  xfilter   syscall filtering with xfilter
  seccomp   syscall filtering with seccomp
EOF
}

main()
{
    # check for command line arguments
    if [[ ${#} == 0 ]]; then
        print_usage
        exit 1
    fi

    # parse command-line arguments
    while [[ ${#} -gt 0 ]]; do

        case "${1}" in
            --install   ) ACTION="do_install"   ;;
            --filter    ) FILTER_TYPE="${2}"; shift  ;;
            --build     ) ACTION="do_build"     ;;
            --enforce   ) ACTION="do_enforce"   ;;
            --verify    ) ACTION="do_verify"    ;;
            --benchmark ) ACTION="do_benchmark" ;;
            --iters     ) ITERS="${2}"; shift   ;;
            --effectiveness ) ACTION="do_effectiveness" ;;
            --clean     ) ACTION="do_clean"     ;;
            -h | --help ) ACTION="print_usage"  ;;
            *           ) do_error "Invalid argument: ${1}." ;;
        esac
        shift

    done

    # argument validation
    [[ -z ${ACTION} ]] && do_error "No action provided."

    [[ ${FILTER_TYPE} != "vanilla"
        && ${FILTER_TYPE} != "xfilter"
        && ${FILTER_TYPE} != "seccomp" ]] && do_error "Invalid filter type."

    [[ ${ACTION} == "do_benchmark"
        && ${ITERS} -lt 1 ]] && do_error "Invalid size."

    # do the action
    ${ACTION}
}
main ${@}
