#!/bin/bash

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

SRC="${1:?"Source argument not provided"}"
DST="${2:?"Destination argument not provided"}"

# mount the rootfs image
sudo mkdir -p "${MOUNT_DIR}"
sudo mount -o loop "${ROOTFS_IMG}" "${MOUNT_DIR}"

# copy over any files
sudo cp -r "${SRC}" "${MOUNT_DIR}/root/${DST}"

# unmount the rootfs image
sudo umount "${MOUNT_DIR}"
