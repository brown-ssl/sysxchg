#include <err.h>
#include <errno.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/audit.h>
#include <linux/filter.h>
#include <linux/seccomp.h>
#include <sys/ptrace.h>


int main(int argc, char *argv[])
{
	const char *path;
	FILE *f;
	unsigned short len;

	path = argv[1];

	struct sock_filter filter[] = {
		{seccomp_filter}
	};

	len = (unsigned short)(sizeof(filter) / sizeof(filter[0]));

	if (!(f = fopen(path, "wb")))
		err(1, "failed to open filter file");

	if ((fwrite(&len, sizeof(unsigned short), 1, f) != 1))
		err(1, "failed to write length to file");

	if ((fwrite(filter, sizeof(struct sock_filter), len, f) != len))
		err(1, "failed to write filter to file");

	if ((fclose(f) != 0))
		err(1, "failed to close filter file");

	return 0;
}

