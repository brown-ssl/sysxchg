#!/usr/bin/python3
import array
import argparse
import json
import os
import sys

NR_syscalls = 451
BITS_PER_BYTE = 8


def bits_to_bytes(nbits):
    return (nbits + BITS_PER_BYTE - 1) // BITS_PER_BYTE

class Bitmap:
    def __init__(self, nbits):
        self.nbits  = nbits;
        self.nbytes = bits_to_bytes(self.nbits);
        self.map    = array.array('B')
        self.map.extend((0,) * self.nbytes)

    def set_bit(self, pos):
        self.map[pos // BITS_PER_BYTE] |= (1 << (pos % BITS_PER_BYTE))

    def clear_bit(self, pos):
        self.map[pos // BITS_PER_BYTE] &= ~(1 << (pos % BITS_PER_BYTE))

    def is_set(self, pos):
        return (self.map[pos // BITS_PER_BYTE] & (1 << (pos % BITS_PER_BYTE)))

    def set_bit_indices(self):
        return [i for i in range(self.nbits) if self.is_set(i)]

    def cleared_bit_indices(self):
        return [i for i in range(self.nbits) if not self.is_set(i)]

    def to_c_byte_array(self):
        return ', '.join('0x{:02x}'.format(i) for i in self.map)

    def write(self):
        sys.stdout.buffer.write(self.map)

    def write_template(self, f):
        print(f.replace('{xfilter}', self.to_c_byte_array()))

    def __repr__(self):
        set_bits = self.set_bit_indices();
        idx = 0
        string = ""

        string += "total bits:\t" + str(self.nbits) + "\n"
        string += "total bytes:\t" + str(self.nbytes) + "\n"
        string += "total set bits:\t" + str(len(set_bits)) + "\n"
        string += "set bits:\n  " + str(set_bits) + "\n"
        string += "bitmap:\n  ["
        for k, v in enumerate(self.map):
            # print 8 bytes followed by a newline
            if (idx % 8 == 0):
                string += "\n    {0}:\t{1:0{2}b} ".format(k, v, BITS_PER_BYTE)
            else:
                string += "{0:0{1}b} ".format(v, BITS_PER_BYTE)
            idx += 1
        string += "\n  ]"
        return string

class Filter:
    def __init__(self):
        p = argparse.ArgumentParser()
        p.add_argument('--lib', action='store_true',
                       help='output file for library-based xfilter')
        p.add_argument('policies', nargs='+', help='policy files')
        self.args = p.parse_args()
        self.size = NR_syscalls
        self.map = Bitmap(self.size)

    def generate_bitmap(self):
        policies = self.args.policies
        nums = set()  # syscall set

        for policy in policies:
            with open(policy) as f:
                s = f.read()
                json_in = json.loads(s)
                if 'syscalls' in json_in:
                    nums.update(json_in['syscalls'])
                else:  # we have just a list of syscall numbers
                    nums.update(json_in)

        for i in nums:
            if (i < self.size):
                self.map.set_bit(i)

    def write_bitmap(self):
        if self.args.lib:
            base_dir = os.path.dirname(os.path.realpath(__file__))
            with open(os.path.join(base_dir, 'xfilter_lib_template.c')) as f:
                tfile = f.read()
                self.map.write_template(tfile)
        else:
            self.map.write()

if __name__ == '__main__':
    fltr = Filter()
    fltr.generate_bitmap()
    fltr.write_bitmap()
