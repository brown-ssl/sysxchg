#include <stdio.h>
#include <stdlib.h>
#include <sys/prctl.h>

#define PR_SET_SYSCALLS	65

__attribute__((constructor))
int install_xfilter(void)
{
	unsigned char filter[] = {
		{xfilter}
	};

	if (prctl(PR_SET_NO_NEW_PRIVS, 1, 0, 0, 0)) {
		perror("prctl(PR_SET_NO_NEW_PRIVS, ...)");
		exit(EXIT_FAILURE);
	};

	if (prctl(PR_SET_SYSCALLS, filter, 0, 0, 0) == -1) {
		perror("prctl(PR_SET_SYSCALLS, ...)");
		exit(EXIT_FAILURE);
	}

	return EXIT_SUCCESS;
}
