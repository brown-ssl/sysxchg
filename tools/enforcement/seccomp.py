#!/usr/bin/python3
import sys
import os
import json
import argparse


class SeccompFilterContext:
    def __init__(self):
        p = argparse.ArgumentParser()
        p.add_argument('policies', nargs='+', help='policy files')
        self.args = p.parse_args()

    def generate_seccomp_lib(self, gen_bpf_prog, overriding_policy=None):
        policies = self.args.policies

        text = 'BPF_STMT(BPF_LD | BPF_W | BPF_ABS,\n'\
               '         (offsetof(struct seccomp_data, arch))),\n'\
               'BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, AUDIT_ARCH_X86_64, 1, 0),\n'\
               'BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_KILL),\n'

        if overriding_policy:
            nums = overriding_policy
        else:
            nums = set()
            for policy in policies:
                with open(policy) as f:
                    s = f.read()
                    json_in = json.loads(s)
                    if 'syscalls' in json_in:
                        nums.update(json_in['syscalls'])
                    else:
                        nums.update(json_in)

        nums = sorted(list(nums))
        text += gen_bpf_prog(nums)
        base_dir = os.path.dirname(os.path.realpath(__file__))
        with open(os.path.join(base_dir, 'seccomp_embed_template.c')) as f:
            tfile = f.read()
            print(tfile.replace('{seccomp_filter}', text))


class BPFInst:
    def __init__(self, n, jt, jf, pred):
        self.n = n
        self.jt = jt
        self.jf = jf
        self.pred = pred

    def format_inst(self):
        if self.pred == 'ge':
            return "BPF_JUMP(BPF_JMP | BPF_JGE | BPF_K, {}, {}, {}),".format(
                self.n, self.jt, self.jf)
        elif self.pred == 'eq':
            return "BPF_JUMP(BPF_JMP | BPF_JEQ | BPF_K, {}, {}, {}),".format(
                self.n, self.jt, self.jf)
        else:
            raise ValueError

def gen_bpf_array(nums, prog):
    s = 0
    if len(nums) <= 2:
        if len(nums) == 2:
            prog.append(BPFInst(nums[1], 'ok', 0, 'eq'))
            s += 1
        if len(nums) != 0:
            prog.append(BPFInst(nums[0], 'ok', 'ct', 'eq'))
            s += 1
        return s
    else:
        c = len(prog)
        n = len(nums)
        prog.append(BPFInst(nums[n//2], 0, 0, 'ge'))
        s += gen_bpf_array(nums[:n//2], prog)
        prog[c].jt = s
        s += gen_bpf_array(nums[n//2:], prog)
        if s+1 > 256:
            raise ValueError('Total insns greater than 256')
        return s+1

def gen_bpf_prog(nums):
    text = 'BPF_STMT(BPF_LD | BPF_W | BPF_ABS,\n'\
           '(offsetof(struct seccomp_data, nr))),\n'
    while len(nums) > 0:
        curs, nums = nums[:128], nums[128:]
        prog = []
        gen_bpf_array(curs, prog)
        for i, l in enumerate(reversed(prog)):
            if l.jt == 'ok':
                l.jt = i
            if l.jf == 'ct':
                l.jf = i+1
        text += '\n'.join([l.format_inst() for
                           l in prog])
        text += '\nBPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_ALLOW),\n'
    text += 'BPF_STMT(BPF_RET | BPF_K, SECCOMP_RET_TRAP),\n'
    return text


if __name__ == '__main__':
    ctx = SeccompFilterContext()
    ctx.generate_seccomp_lib(gen_bpf_prog)

