#!/bin/bash
set -e

# import cookie-cutter functions
source "$(dirname "${0}")"/common.sh

clean_kernel()
{
    local config_name="${1}"
    local build_dir="${BUILD_DIR}/${KERNEL_VERSION}-${config_name}"

    rm -rf "${build_dir}"
}

clean_rootfs()
{
    sudo rm -rf "${ROOTFS_BUILD_DIR}"
}

clean_keys()
{
    rm -rf "${KEYS_BUILD_DIR}"
}

clean_tests()
{
    # TODO: implement me!
    echo "TODO: implement me!"
}

clean_bmks()
{
    local bmk="${1}"

    vmstart vanilla

    case "${bmk}" in
    
        spec )
            for ftype in ${INHERITANCE_FILTER_TYPES}; do
                vmcmd "${SPEC_SCRIPT} --filter ${ftype} --clean"
            done
            read -p "Remove the entire SPEC CPU 2017 directory (cpu2017)? " -n 1 -r
            echo
            if [[ ${REPLY} =~ ^[Yy]$ ]]; then
                rm -rf "${BUILD_DIR}/cpu2017"
            fi
            # NOTE: we do not delete the whole cpu2017 directory as this
            # should never really be necessary.
            ;;

        sqlite )
            # TODO: implement me!
            echo "TODO: implement me!"
            ;;

        pash )
            # TODO: implement me!
            echo "TODO: implement me!"
            ;;

        * )
            vmstop
            do_error "Invalid benchmark: ${bmk}."
            ;;
    esac

    vmstop
}

print_usage()
{
    cat 1>&2 <<EOF
Usage: $(ps -o args= ${PPID} | cut -d' ' -f2) clean [options]

Options:
  help              print this help menu
  all               clean all kernels and rootfs
  kernel <config>   clean kernel with config <config>
  rootfs            clean rootfs, including all tests and benchmarks
  tests             clean all tests
  benchmark <bmk>   clean benchmark <bmk>

Configs <config>:
  vanilla           Linux kernel default (baseline) configuration
  inheritance       Linux kernel inheritance model configuration (exec filter and xfilter support)
  exchange          Linux kernel exchange model configuration (full SysXCHG)

Benchmarks <bmk>:
  spec              SPEC CPU 2017
  sqlite            SQLite Speedtest
  pash              a subset of PaSH benchmarks
EOF
}

main()
{
    local config bmk

    # check for command line arguments
    if [[ ${#} == 0 ]]; then
        print_usage
        exit 1
    fi

    case "${1}" in
        all )
            sudo rm -rf "${BUILD_DIR}"
            ;;

        kernel )
            config="${2}"
            verify_config_opt "${config}"
            clean_kernel "${config}"
            ;;

        rootfs )
            clean_rootfs
            ;;

        keys )
            clean_keys
            ;;

        tests )
            clean_tests
            ;;

        benchmark )
            bmk="${2}"
            verify_bmk_opt "${bmk}"
            clean_benchmark "${bmk}"
            ;;

        help )
            print_usage
            ;;

        * )
            do_error "Invalid argument: ${1}."
            ;;
    esac
}
main ${@}
