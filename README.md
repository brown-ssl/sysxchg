<div align="center">

# SysXCHG

#### Refining Privilege with Adaptive System Call Filters

</div>

We present the design, implementation, and evaluation of SysXCHG: a system call (syscall) filtering enforcement mechanism that enables programs to run in accordance to the principle of least privilege. In contrast to the current, hierarchical design of seccomp-BPF, which does not allow a program to run with a different set of allowed syscalls than its descendants, SysXCHG enables applications to run with "tight" syscall filters, uninfluenced by any future-executed (sub-)programs, by allowing filters to be dynamically exchanged at runtime during `execve[at]`. As a part of SysXCHG, we also present xfilter: a mechanism for fast filtering using a process-specific view of the kernel's syscall table where filtering is performed. In our evaluation of SysXCHG, we found that our filter exchanging design is performant, incurring &le;1.71% slowdown on real-world programs in the PaSH benchmark suite, as well as effective, blocking vast amounts of extraneous functionality, including security-critical syscalls, which the current design of seccomp-BPF is unable to.

Further information about the design and implementation of SysXCHG can be found in our [paper](./misc/sysxchg.pdf) presented at [CCS 2023](https://www.sigsac.org/ccs/CCS2023/program.html).

```
@inproceedings{sysxchg_ccs2023,
    title       = {{SysXCHG: Refining Privilege with Adaptive System Call Filters}},
    author      = {Gaidis, Alexander J. and Atlidakis, Vaggelis and Kemerlis, Vasileios P.},
    booktitle   = {Proceedings of the ACM SIGSAC Conference on Computer and Communications Security (CCS)},
    pages       = {XXX--YYY},
    year        = {2023}
}
```

## Table of Contents

 - [Directory Structure](#directory-structure)
 - [Supported Environment](#supported-environment)
 - [Major Claims & Experiments](#major-claims-&-experiments)
 - [Setup](#setup)
 - [Build](#build)
 - [Test Functionality](#test-functionality)
 - [Evaluate](#evaluate)
 - [Debugging](#debugging)
 - [Cleaning](#cleaning)
 - [License](#license)

## Directory Structure

 - [`bmk-src`](./bmk-src): benchmark source code
 - [`configs`](./configs): kernel (and benchmark) configurations
 - [`linux-6.0.8`](./linux-6.0.8): SysXCHG Linux kernel
 - [`misc`](./misc): Miscellaneous items
 - [`policies`](./policies): System call policies
 - [`tests`](./tests): A series of functionality tests for SysXCHG
 - [`tools`](./tools): Tools for building and running SysXCHG

## Supported Environment

This prototype of SysXCHG assumes an x86_64 machine running Debian (Bullseye or higher) Linux with at least 8GB of RAM, 4 available CPU cores, and approximately 60GB of storage space. While we use QEMU/KVM to recreate the environment used in the paper, benchmark numbers were actually collected on bare-metal. Specifically, we used a machine with an Intel Xeon W-2145 8-core (16-thread) processor and 64GB of DDR4 memory running Debian v11 (Bullseye) Linux with kernel v6.0.8.

## Major Claims & Experiments

#### Performance

 - **(C1):** xfilter (using exec filters) performs equal to or better than seccomp-BPF (also using exec filters) under the __inheritance model__. This is proven by the experiment (E1) illustrated in Secion 6.1.1 and Tables 1-2 of the paper.
 - **(E1):** Benchmark the performance of SPEC CPU 2017 and SQLite under the inheritance model (kernel) for vanilla (baseline), xfilter, and seccomp-BPF enforcement types (all using exec filters).

 - **(C2):** xfilter (using exec filters) performs equal to or better than seccomp-BPF (also using exec filters) under the __exchange model__ (i.e., dynamic filter switching) where overheads for both mechanisms are low, averaging &lt;3% for the PaSH macrobenchmarks.  This is proven by the experiment (E2) described in Section 6.1.2 of the paper, whose results are further illustrated in Table 3.
 - **(E2):** Benchmark the performance of PaSH under the exchange model (kernel) for vanilla (baseline), xfilter, and seccomp-BPF enforcement types (all using exec filters).

#### Effectiveness
 - **(C3):** SysXCHG can reduce both the kernel's attack surface and an attacker's capabilities post-exploitation by employing the exchange model. This is proven by the experiment (E3) described in Section 6.2 of the paper, whose results are reported in Table 4.
 - **(E3):** Perform an analysis of PaSH benchmark programs' syscall capabilities under the inheritance and exchange models.


## Setup

To reproduce the main claims of the paper, we have recreated the bare-metal benchmarking environment we used in the paper with virtualization, specifically, [QEMU](https://www.qemu.org/)/[KVM](https://www.linux-kvm.org/page/Main_Page). At a high-level, we create a single root filesystem that contains all of the benchmarks and scripts required to evaluate SysXCHG. Then, we build three custom Linux kernels which implement SysXCHG's filtering models (vanilla, inheritance, and exchange). Finally, we run QEMU/KVM using these components to get an evaluation environment that we can use to reproduce SysXCHG's results.

### Packages

Various packages are required to use QEMU as well as build the Linux kernel. On Debian Bullseye, the required packages can be installed as follows:

```shell
sudo apt-get install build-essential libncurses-dev bison flex bc libssl-dev libelf-dev zstd qemu-system-x86 debootstrap wget gdb openssh-client
```

On other distributions, the required package names may vary.

### Virtualization

As mentioned above, we use QEMU/KVM to recreate the major claims from the paper in a portable manner. In order to run the virtual machines:

 - Ensure that the user in the `kvm` group
 - Ensure that `/dev/kvm` exists

<details>
<summary>Troubleshooting</summary>

To troubleshoot issues with KVM, the tool `kvm-ok` can be useful. Install it with:

```shell
sudo apt-get install cpu-checker
```

If things are successful, running `kvm-ok` should output:

```
INFO: /dev/kvm exists
KVM acceleration can be used
```
</details>


### Bash Completion

We have created a script to provide command-line completion for `bash` to streamline interacting with our infrastructure. To use it, run the following from the root of the repository:

```shell
export PATH="$PWD/tools:$PATH"
source ./misc/bash_completion.sh
```

## Build

### Use Pre-built Environment

For convenience, we provide a pre-built environment (e.g., kernels, rootfs, etc.). To use it, simply decompress `prebuilt.tar.gz`:

```shell
tar -xzf prebuilt.tar.gz
```

The result should be a `build` directory in the root of this repository containing several directories whose contents is further described below.


### Build from Scratch

We also provide scripts to build the entire environment from scratch. From the root of the repository, run:

```shell
./tools/sysxchg build all
```

This will:

 1. Create a root filesystem from [syzkaller](https://github.com/google/syzkaller) scripts.
 2. Create keys for IMA/EVM.
 3. Build three Linux kernels (v6.0.8):
    - `vanilla`: basic (no modifications)
    - `inheritance`: support for exec filters and xfilter under the inheritance model
    - `exchange`: support for exec filters and xfilter under the exchange model
 4. Build the test suite.
 5. Build and enforce the benchmarks:
    - SPEC CPU 2017 (if available)
    - SQLite
    - PaSH

All of the above should happen automatically without requiring input from the user. Depending on the machine used to build things, this step can take anywhere from one to several hours. Notably, `sysxchg build` can also perform the tasks above individually. For more information regarding this, run:

```shell
./tools/sysxchg build help
```

## Test Functionality

We have provided two sets of tests that demonstrate SysXCHG's functionality and verify that it was installed correctly. The two test sets and their descriptions are as follows:

#### Inheritance Tests

These tests are run under the inheritance kernel (i.e., no dynamic filter switching on `execve`) and demonstrate that exec filters (i.e., those embedded in the ELF file) work with xfilter and Seccomp-BPF and that manual filters (i.e., those installed with `prctl`) work with xfilter.
 
<details>
<summary>Individual test descriptions</summary>

 1. `1-exec-xfilter-allow`: Tests installing an exec xfilter and ensuring xfilter subsequently allows a syscall.
 2. `2-exec-xfilter-deny`: Tests installing an exec xfilter and ensuring xfilter subsequently blocks a syscall.
 3. `3-exec-seccomp-allow`: Tests installing an exec Seccomp-BPF filter and ensuring Seccomp-BPF subsequently allows a syscall.
 4. `4-exec-seccomp-deny`: Tests installing an exec Seccomp-BPF filter and ensuring Seccomp-BPF subsequently blocks a syscall.
 5. `5-manual-xfilter-allow`: Tests installing an xfilter manually (via `prctl`) and ensuring xfilter subsequently allows a syscall.
 6. `6-manual-xfilter-deny`: Tests installing an xfilter manually (via `prctl`) and ensuring xfilter subsequently blocks a syscall.
 7. `7-exec-xfilter-{1, 2}`: Tests that the inheritance model is applied across `execve` for exec xfilters by checking whether certain syscalls are allowed or blocked.
 8. `8-exec-seccomp-{1, 2}`: Tests that the inheritance model is applied across `execve` for exec Seccomp-BPF filters by checking whether certain syscalls are allowed or blocked.

</details>

To run the inheritance tests, execute the following:

```shell
./tools/sysxchg run tests inheritance
```

<span style="color:orange">NOTE: this should be run from the root of this repository **on the host machine**.</span>

<details>
<summary>Example inheritance test output</summary>

The expected output (including descriptions) of the inheritance tests is as follows (note that `->` represents an `execve` relationship):

```
=============================
====[ Inheritance Tests ]====
=============================
--[ Test 1: ./inheritance/bin/1-exec-xfilter-allow
[Description] Testing exec xfilter with allowed syscall

[PASS][syscall getuid:#102 allowed] 1-exec-xfilter-allow.c

--[ Test 2: ./inheritance/bin/2-exec-xfilter-deny
[Description] Testing exec xfilter with blocked syscall

[PASS][syscall getuid:#102 blocked] 2-exec-xfilter-deny.c

--[ Test 3: ./inheritance/bin/3-exec-seccomp-allow
[Description] Testing exec seccomp with allowed syscall

[PASS][syscall getuid:#102 allowed] 3-exec-seccomp-allow.c

--[ Test 4: ./inheritance/bin/4-exec-seccomp-deny
[Description] Testing exec seccomp with blocked syscall

[PASS][syscall getuid:#102 blocked] 4-exec-seccomp-deny.c

--[ Test 5: ./inheritance/bin/5-manual-xfilter-allow
[Description] Testing manual xfilter with allowed syscall

[PASS][syscall getuid:#102 allowed] 5-manual-xfilter-allow.c

--[ Test 6: ./inheritance/bin/6-manual-xfilter-deny
[Description] Testing manual xfilter with blocked syscall

[PASS][syscall getuid:#102 blocked] 6-manual-xfilter-deny.c

--[ Test 7: ./inheritance/bin/7-exec-xfilter-1
[Description] Testing exec xfilter -> exec xfilter

[PASS][syscall getgid:#104 allowed] 7-exec-xfilter-1.c
[PASS][syscall geteuid:#107 allowed] 7-exec-xfilter-1.c
[PASS][syscall getuid:#102 blocked] 7-exec-xfilter-1.c
[PASS][syscall getgid:#104 allowed] 7-exec-xfilter-2.c
[PASS][syscall geteuid:#107 blocked] 7-exec-xfilter-2.c
[PASS][syscall getuid:#102 blocked] 7-exec-xfilter-2.c

--[ Test 8: ./inheritance/bin/8-exec-seccomp-1
[Description] Testing exec seccomp filter -> exec seccomp filter

[PASS][syscall getgid:#104 allowed] 8-exec-seccomp-1.c
[PASS][syscall geteuid:#107 allowed] 8-exec-seccomp-1.c
[PASS][syscall getuid:#102 blocked] 8-exec-seccomp-1.c
[PASS][syscall getgid:#104 allowed] 8-exec-seccomp-2.c
[PASS][syscall geteuid:#107 blocked] 8-exec-seccomp-2.c
[PASS][syscall getuid:#102 blocked] 8-exec-seccomp-2.c

[    8.287019] reboot: Power down
[+][2023/10/25 @ 04:50:17]  Stopped VM.
```

</details>


#### Exchange Tests

These tests are run under the exchange kernel with IMA appraisal enabled (i.e., full SysXCHG) and demonstrate dynamic filter switching in several scenarios across xfilter and Seccomp-BPF. Additionally, these tests verify that binary signing with IMA/EVM works.

<details>
<summary>Individual test descriptions</summary>

 1. `1-exec-xfilter-{1, 2}`: Tests that the exchange model works across `execve` for exec xfilters by checking whether certain syscalls are allowed or blocked.
 2. `2-exec-seccomp-{1, 2}`: Tests that the exchange model works across `execve` for exec Seccomp-BPF filters by checking whether certain syscalls are allowed or blocked.
 3. `3-manual-xfilter-1` & `3-exec-xfilter-2`: Tests the interplay of manual and exec xfilters across `execve` under the exchange model by checking whether certain syscalls are allowed or blocked.
 4. `4-manual-xfilter-1` & `4-exec-seccomp-2`: Tests the interplay of a manual xfilter and exec Seccomp-BPF filter across `execve` under the exchange model by checking whether certain syscalls are allowed or blocked.
 5. `5-manual-xfilter-{1, 2}`: Tests that the exchange model works across `execve` for manual xfilters by checking whether certain syscalls are allowed or blocked.

</details>

To run the exchange tests, execute the following:

```shell
./tools/sysxchg run tests exchange 
```

<span style="color:orange">NOTE: this should be run from the root of this repository **on the host machine**.</span>

<details>
<summary>Example exchange test output</summary>

The expected output (including descriptions) of the exchange tests is as follows (note that `->` represents an `execve` relationship):

```
[+][2023/10/25 @ 01:39:16]  IMA/EVM enabled.
[+][2023/10/25 @ 01:39:16]  IMA/EVM is indeed enabled.
======================
====[ Xchg Tests ]====
======================
--[ Test 1: ./exchange/bin/1-exec-xfilter-1
[Description] Testing exec xfilter -> exec xfilter

[PASS][syscall getgid:#104 allowed] 1-exec-xfilter-1.c
[PASS][syscall geteuid:#107 allowed] 1-exec-xfilter-1.c
[PASS][syscall getuid:#102 blocked] 1-exec-xfilter-1.c
[PASS][syscall getgid:#104 allowed] 1-exec-xfilter-2.c
[PASS][syscall geteuid:#107 blocked] 1-exec-xfilter-2.c
[PASS][syscall getuid:#102 allowed] 1-exec-xfilter-2.c

--[ Test 2: ./exchange/bin/2-exec-seccomp-1
[Description] Testing exec seccomp filter -> exec seccomp filter

[PASS][syscall getgid:#104 allowed] 2-exec-seccomp-1.c
[PASS][syscall geteuid:#107 allowed] 2-exec-seccomp-1.c
[PASS][syscall getuid:#102 blocked] 2-exec-seccomp-1.c
[PASS][syscall getgid:#104 allowed] 2-exec-seccomp-2.c
[PASS][syscall geteuid:#107 blocked] 2-exec-seccomp-2.c
[PASS][syscall getuid:#102 allowed] 2-exec-seccomp-2.c

--[ Test 3: ./exchange/bin/3-manual-xfilter-1
[Description] Testing manual xfilter -> exec xfilter

[PASS][syscall getgid:#104 allowed] 3-manual-xfilter-1.c
[PASS][syscall geteuid:#107 allowed] 3-manual-xfilter-1.c
[PASS][syscall getuid:#102 blocked] 3-manual-xfilter-1.c
[PASS][syscall getgid:#104 allowed] 3-exec-xfilter-2.c
[PASS][syscall geteuid:#107 blocked] 3-exec-xfilter-2.c
[PASS][syscall getuid:#102 blocked] 3-exec-xfilter-2.c

--[ Test 4: ./exchange/bin/4-manual-xfilter-1
[Description] Testing manual xfilter -> exec seccomp filter

[PASS][syscall getgid:#104 allowed] 4-manual-xfilter-1.c
[PASS][syscall geteuid:#107 allowed] 4-manual-xfilter-1.c
[PASS][syscall getuid:#102 blocked] 4-manual-xfilter-1.c
[PASS][syscall getgid:#104 allowed] 4-exec-seccomp-2.c
[PASS][syscall geteuid:#107 blocked] 4-exec-seccomp-2.c
[PASS][syscall getuid:#102 allowed] 4-exec-seccomp-2.c

--[ Test 5: ./exchange/bin/5-manual-xfilter-1
[Description] Testing manual xfilter -> manual xfilter

[PASS][syscall getgid:#104 allowed] 5-manual-xfilter-1.c
[PASS][syscall geteuid:#107 allowed] 5-manual-xfilter-1.c
[PASS][syscall getuid:#102 blocked] 5-manual-xfilter-1.c
[PASS][syscall getgid:#104 allowed] 5-manual-xfilter-2.c
[PASS][syscall geteuid:#107 blocked] 5-manual-xfilter-2.c
[PASS][syscall getuid:#102 blocked] 5-manual-xfilter-2.c

[    8.908401] reboot: Power down
[+][2023/10/25 @ 01:39:19]  Stopped VM.
```

</details>


## Evaluate

### Performance

We have chosen a representative sample of the benchmarks used in the SysXCHG paper to verify SysXCHG's performance claims. Specifically, we include the following benchmarks that do not require performance tuning or a large amount of resources:

 - SPEC CPU 2017 C/C++ intspeed and fpspeed benchmark suite
 - SQLite `speedtest1` (in-tree) benchmark
 - A collection of PaSH benchmarks, namely:
    - Bash oneliners
    - Natural Language Processing
    - NOAA Weather Analysis
    - PCAP Log Analysis
    - Encryption
    - Compression

All benchmarks are built three times with three different filtering enforcement mechanisms based around exec filters, namely: (1) no filtering (vanilla/baseline), (2) xfilter, and (3) seccomp-BPF. Importantly, SPEC CPU 2017 and SQLite are run under the inheritance model kernel (as they do not perform many `execve` syscalls) while PaSH is run under the exchange model kernel with IMA appraisal enabled. As such, PaSH binaries not only have their respective syscall filters embedded in them, but they are also signed with IMA/EVM.

The three commands given below run the respective performance benchmarks.

```shell
./tools/sysxchg run performance spec
./tools/sysxchg run performance sqlite
./tools/sysxchg run performance pash
```

<span style="color:orange">NOTE: this should be run from the root of this repository **on the host machine**.</span>

<span style="color:orange">NOTE: Due to licensing restrictions we do not provide SPEC CPU 2017's source code; see below for more information.</span>

<details>
<summary>Installing/running SPEC CPU 2017</summary>

Due to licensing restrictions we do not provide the source code for SPEC CPU 2017. However, we do provide automation for building and enforcing it when it is available. If building the benchmark environment from scratch, the SPEC CPU 2017 source code simply needs to be placed in the `bmk-src` directory under the name `cpu2017`. Once this is done, the build command (i.e., `sysxchg build all`) will automatically detect and integrate SPEC CPU 2017 into the build process. If instead the pre-built benchmark environment is being used, the SPEC CPU 2017 source code must be copied into the rootfs image and then built and enforced.  To do the copying, we provide a script that can be invoked from the `tools` directory as follows:

```shell
./rootfs-cp.sh <src> "bmk-src/cpu2017"
```

After this, the remaining steps can be completed by calling the following:

```shell
./sysxchg build benchmark spec
```

</details>

For 10 runs of each benchmark, one should expect SPEC CPU 2017 to take between 1-2 hours, SQLite to take less than 10 minutes, and PaSH to take around 5 hours.

The results for these benchmarks are collected in the `results` directory in the root of the repository. Importantly, the results are copied from the VM to the host **after** completion---until then the `results` directory is not touched. There are a collection of scripts in `tools/analysis` that will process the results and print runtime averages. For example:

```shell
./tools/analysis/analyze-spec.sh
./tools/analysis/analyze-sqlite.sh
./tools/analysis/analyze-pash.sh
```

Per our claims, you should see that xfilter performs equal to or better than seccomp-BPF across everything and that xfilter and seccomp-BPF's overhead should be relatively close to the baseline measurement (see Table 1-2 of the paper).

<details>
<summary>Changing benchmark parameters</summary>

The parameters for each benchmark (e.g., benchmark iterations) can be changed by editing the `run_performance()` function in the `tools/run.sh` script. Parameter options for each benchmark can be seen by running one of the following:

```shell
./tools/spec.sh --help
./tools/sqlite.sh --help
./tools/pash.sh --help
```

Then, the lines containing `vmcmd` can be adjusted as desired. For example, to run 5 iterations of the `ref` dataset for SPEC CPU 2017 (instead of the default 10 iterations of `test`), lines 42, 49, and 52 should be edited to be (respectively):

```shell
...
vmcmd "${SPEC_SCRIPT} --filter vanilla --benchmark --iters 5 --size ref"
...
vmcmd "${SPEC_SCRIPT} --filter xfilter --benchmark --iters 5 --size ref"
...
vmcmd "${SPEC_SCRIPT} --filter seccomp --benchmark --iters 5 --size ref"
...
```

</details>

### Effectiveness

An effectiveness analysis of the PaSH benchmarks can be run as follows:

```shell
./tools/sysxchg run effectiveness
```

<span style="color:orange">NOTE: this should be run from the root of this repository **on the host machine**.</span>

One should expect the effectiveness evaluation to take between 1-2 hours.

The results for this evaluation are collected in the `results/effectiveness` directory. Importantly, the results are copied from the VM to the host **after** completion---until then the `results` directory is not touched. Each result file in this directory lists the following (click each for example output from the `shortest-scripts` benchmark and more verbose descriptions):

<details>
<summary>strace output</summary>

This is the `strace` output of running the benchmark which is used to gather the results. Example output:

```
2683  1699296453.483521 [  59] execve("./shortest-scripts.sh", ["./shortest-scripts.sh", ""], 0x7ffeaa070188 /* 56 vars */) = 0
2683  1699296453.485355 [  59] execve("/usr/bin/bash", ["bash", "./shortest-scripts.sh", ""], 0x7ffd2328bb50 /* 56 vars */) = 0
2683  1699296453.490054 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f5ba629ea10) = 2684
2683  1699296453.490355 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f5ba629ea10) = 2685
2683  1699296453.490707 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f5ba629ea10) = 2686
2683  1699296453.491127 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f5ba629ea10) = 2687
2683  1699296453.491669 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f5ba629ea10) = 2688
2683  1699296453.492291 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f5ba629ea10) = 2689
2684  1699296453.492348 [  59] execve("/usr/bin/cat", ["cat", "/root/tools/../build/pash/onelin"...], 0x562aba75db40 /* 56 vars */) = 0
2685  1699296453.492868 [  59] execve("/usr/bin/xargs", ["xargs", "file"], 0x562aba75db40 /* 56 vars */) = 0
2683  1699296453.492998 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f5ba629ea10) = 2690
2683  1699296453.493742 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f5ba629ea10) = 2691
2686  1699296453.493839 [  59] execve("/usr/bin/grep", ["grep", "shell script"], 0x562aba75db40 /* 56 vars */) = 0
2687  1699296453.494602 [  59] execve("/usr/bin/cut", ["cut", "-d:", "-f1"], 0x562aba75db40 /* 56 vars */) = 0
2688  1699296453.495279 [  59] execve("/usr/bin/xargs", ["xargs", "-L", "1", "wc", "-l"], 0x562aba75db40 /* 56 vars */) = 0
2689  1699296453.496082 [  59] execve("/usr/bin/grep", ["grep", "-v", "^0$"], 0x562aba75db40 /* 56 vars */) = 0
2684  1699296453.497129 [ 231] +++ exited with 0 +++
2690  1699296453.497289 [  59] execve("/usr/bin/sort", ["sort", "-n"], 0x562aba75db40 /* 56 vars */) = 0
2691  1699296453.497865 [  59] execve("/usr/bin/head", ["head", "-15"], 0x562aba75db40 /* 56 vars */) = 0
2685  1699296453.498192 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7f463b3fc850) = 2692
2692  1699296453.499345 [  59] execve("/usr/bin/file", ["file", "/usr/bin/[", "/usr/bin/2to3-2.7", "/usr/bin/411toppm", "/usr/bin/4ti2-circuits", "/usr/bin/4ti2-genmodel", "/usr/bin/4ti2-gensymm", "/usr/bin/4ti2-graver", "/usr/bin/4ti2-groebner", "/usr/bin/4ti2-hilbert", "/usr/bin/4ti2-markov", "/usr/bin/4ti2-minimize", "/usr/bin/4ti2-normalform", "/usr/bin/4ti2-output", "/usr/bin/4ti2-ppi", "/usr/bin/4ti2-qsolve", "/usr/bin/4ti2-rays", "/usr/bin/4ti2-walk", "/usr/bin/4ti2-zbasis", "/usr/bin/4ti2-zsolve", "/usr/bin/a2ping", "/usr/bin/a5toa4", "/usr/bin/aa-enabled", "/usr/bin/aa-exec", "/usr/bin/ack", "/usr/bin/aclocal", "/usr/bin/aclocal-1.16", "/usr/bin/acpi", "/usr/bin/acpi_listen", "/usr/bin/acyclic", "/usr/bin/addpart", "/usr/bin/add-patch", ...], 0x7fffe9802540 /* 56 vars */) = 0
2692  1699296453.710559 [ 231] +++ exited with 0 +++
2685  1699296453.710625 [  61] --- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=2692, si_uid=0, si_status=0, si_utime=4, si_stime=4} ---
2685  1699296453.710860 [ 231] +++ exited with 0 +++
2686  1699296453.711088 [ 231] +++ exited with 0 +++
2687  1699296453.711142 [ 231] +++ exited with 0 +++
2688  1699296453.711069 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD <unfinished ...>
, child_tidptr=0x7fce18348850)          = 2693
2693  1699296453.711585 [  59] execve("/usr/bin/wc", ["wc", "-l", "/usr/bin/apt-key"], 0x7ffdc9ae03b8 /* 56 vars */) = 0
2693  1699296453.713338 [ 231] +++ exited with 0 +++
2688  1699296453.713361 [  61] --- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=2693, si_uid=0, si_status=0, si_utime=0, si_stime=0} ---
2688  1699296453.713433 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7fce18348850) = 2694
2694  1699296453.713853 [  59] execve("/usr/bin/wc", ["wc", "-l", "/usr/bin/bashbug"], 0x7ffdc9ae03b8 /* 56 vars */) = 0
2694  1699296453.715080 [ 231] +++ exited with 0 +++
2688  1699296453.715106 [  61] --- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=2694, si_uid=0, si_status=0, si_utime=0, si_stime=0} ---
2688  1699296453.715176 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7fce18348850) = 2695
2695  1699296453.715613 [  59] execve("/usr/bin/wc", ["wc", "-l", "/usr/bin/c89-gcc"], 0x7ffdc9ae03b8 /* 56 vars */) = 0
2695  1699296453.716840 [ 231] +++ exited with 0 +++
2688  1699296453.716866 [  61] --- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=2695, si_uid=0, si_status=0, si_utime=0, si_stime=0} ---
2688  1699296453.716936 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7fce18348850) = 2696
2696  1699296453.717477 [  59] execve("/usr/bin/wc", ["wc", "-l", "/usr/bin/c99-gcc"], 0x7ffdc9ae03b8 /* 56 vars */) = 0
2696  1699296453.718998 [ 231] +++ exited with 0 +++
2688  1699296453.719033 [  61] --- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=2696, si_uid=0, si_status=0, si_utime=0, si_stime=0} ---
2688  1699296453.719107 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7fce18348850) = 2697
2697  1699296453.719527 [  59] execve("/usr/bin/wc", ["wc", "-l", "/usr/bin/catchsegv"], 0x7ffdc9ae03b8 /* 56 vars */) = 0
2697  1699296453.720728 [ 231] +++ exited with 0 +++
2688  1699296453.720759 [  61] --- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=2697, si_uid=0, si_status=0, si_utime=0, si_stime=0} ---
2688  1699296453.720831 [  56] clone(child_stack=NULL, flags=CLONE_CHILD_CLEARTID|CLONE_CHILD_SETTID|SIGCHLD, child_tidptr=0x7fce18348850) = 2698
2698  1699296453.721314 [  59] execve("/usr/bin/wc", ["wc", "-l", "/usr/bin/dpkg-maintscript-helper"], 0x7ffdc9ae03b8 /* 56 vars */) = 0
2698  1699296453.722594 [ 231] +++ exited with 0 +++
2688  1699296453.722624 [  61] --- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=2698, si_uid=0, si_status=0, si_utime=0, si_stime=0} ---
2688  1699296453.722887 [ 231] +++ exited with 0 +++
2689  1699296453.723003 [ 231] +++ exited with 0 +++
2690  1699296453.723132 [ 231] +++ exited with 0 +++
2691  1699296453.723256 [ 231] +++ exited with 0 +++
2683  1699296453.723399 [  14] --- SIGCHLD {si_signo=SIGCHLD, si_code=CLD_EXITED, si_pid=2684, si_uid=0, si_status=0, si_utime=0, si_stime=0} ---
2683  1699296453.723645 [ 231] +++ exited with 0 +++
[DBG] Searching for interpreter for ./shortest-scripts.sh
[DBG] Using interpreter: ./shortest-scripts.sh => /usr/bin/bash
[DBG] EXEC: { pid = 2683, ppid = 0, prog = ./shortest-scripts.sh, ctime = 0, etime = 1699296453.483521 }
[DBG] EXEC: { pid = 2683, ppid = 0, prog = /usr/bin/bash, ctime = 0, etime = 1699296453.485355 }
[DBG] FORK: { pid = 2684, ppid = 2683, prog = /usr/bin/bash, ctime = 1699296453.490054, etime = 0 }
[DBG] FORK: { pid = 2685, ppid = 2683, prog = /usr/bin/bash, ctime = 1699296453.490355, etime = 0 }
[DBG] FORK: { pid = 2686, ppid = 2683, prog = /usr/bin/bash, ctime = 1699296453.490707, etime = 0 }
[DBG] FORK: { pid = 2687, ppid = 2683, prog = /usr/bin/bash, ctime = 1699296453.491127, etime = 0 }
[DBG] FORK: { pid = 2688, ppid = 2683, prog = /usr/bin/bash, ctime = 1699296453.491669, etime = 0 }
[DBG] FORK: { pid = 2689, ppid = 2683, prog = /usr/bin/bash, ctime = 1699296453.492291, etime = 0 }
[DBG] EXEC: { pid = 2684, ppid = 2683, prog = /usr/bin/cat, ctime = 1699296453.490054, etime = 1699296453.492348 }
[DBG] EXEC: { pid = 2685, ppid = 2683, prog = /usr/bin/xargs, ctime = 1699296453.490355, etime = 1699296453.492868 }
[DBG] FORK: { pid = 2690, ppid = 2683, prog = /usr/bin/bash, ctime = 1699296453.492998, etime = 0 }
[DBG] FORK: { pid = 2691, ppid = 2683, prog = /usr/bin/bash, ctime = 1699296453.493742, etime = 0 }
[DBG] EXEC: { pid = 2686, ppid = 2683, prog = /usr/bin/grep, ctime = 1699296453.490707, etime = 1699296453.493839 }
[DBG] EXEC: { pid = 2687, ppid = 2683, prog = /usr/bin/cut, ctime = 1699296453.491127, etime = 1699296453.494602 }
[DBG] EXEC: { pid = 2688, ppid = 2683, prog = /usr/bin/xargs, ctime = 1699296453.491669, etime = 1699296453.495279 }
[DBG] EXEC: { pid = 2689, ppid = 2683, prog = /usr/bin/grep, ctime = 1699296453.492291, etime = 1699296453.496082 }
[DBG] EXEC: { pid = 2690, ppid = 2683, prog = /usr/bin/sort, ctime = 1699296453.492998, etime = 1699296453.497289 }
[DBG] EXEC: { pid = 2691, ppid = 2683, prog = /usr/bin/head, ctime = 1699296453.493742, etime = 1699296453.497865 }
[DBG] FORK: { pid = 2692, ppid = 2685, prog = /usr/bin/xargs, ctime = 1699296453.498192, etime = 0 }
[DBG] EXEC: { pid = 2692, ppid = 2685, prog = /usr/bin/file, ctime = 1699296453.498192, etime = 1699296453.499345 }
[DBG] FORK: { pid = 2693, ppid = 2688, prog = /usr/bin/xargs, ctime = 1699296453.711069, etime = 0 }
[DBG] EXEC: { pid = 2693, ppid = 2688, prog = /usr/bin/wc, ctime = 1699296453.711069, etime = 1699296453.711585 }
[DBG] FORK: { pid = 2694, ppid = 2688, prog = /usr/bin/xargs, ctime = 1699296453.713433, etime = 0 }
[DBG] EXEC: { pid = 2694, ppid = 2688, prog = /usr/bin/wc, ctime = 1699296453.713433, etime = 1699296453.713853 }
[DBG] FORK: { pid = 2695, ppid = 2688, prog = /usr/bin/xargs, ctime = 1699296453.715176, etime = 0 }
[DBG] EXEC: { pid = 2695, ppid = 2688, prog = /usr/bin/wc, ctime = 1699296453.715176, etime = 1699296453.715613 }
[DBG] FORK: { pid = 2696, ppid = 2688, prog = /usr/bin/xargs, ctime = 1699296453.716936, etime = 0 }
[DBG] EXEC: { pid = 2696, ppid = 2688, prog = /usr/bin/wc, ctime = 1699296453.716936, etime = 1699296453.717477 }
[DBG] FORK: { pid = 2697, ppid = 2688, prog = /usr/bin/xargs, ctime = 1699296453.719107, etime = 0 }
[DBG] EXEC: { pid = 2697, ppid = 2688, prog = /usr/bin/wc, ctime = 1699296453.719107, etime = 1699296453.719527 }
[DBG] FORK: { pid = 2698, ppid = 2688, prog = /usr/bin/xargs, ctime = 1699296453.720831, etime = 0 }
[DBG] EXEC: { pid = 2698, ppid = 2688, prog = /usr/bin/wc, ctime = 1699296453.720831, etime = 1699296453.721314 }
```

</details>
<details>
<summary>execve relationships</summary>

This contains the information about what programs `execve` what other programs. Here, an `execve` is symbolized with an `>`. Example output:

```
===============================================================================
====[[ Execve Relationships ]]=================================================

/usr/bin/bash
/usr/bin/bash > /usr/bin/cat
/usr/bin/bash > /usr/bin/cut
/usr/bin/bash > /usr/bin/grep
/usr/bin/bash > /usr/bin/sort
/usr/bin/bash > /usr/bin/head
/usr/bin/bash > /usr/bin/xargs
/usr/bin/bash > /usr/bin/xargs > /usr/bin/wc
/usr/bin/bash > /usr/bin/xargs > /usr/bin/file
```

</details>
<details>
<summary>Program list</summary>

This contains a list of all programs executed by the benchmark. Example output:

```
===============================================================================
====[[ Raw Program List ]]=====================================================

./shortest-scripts.sh
/usr/bin/bash
/usr/bin/cat
/usr/bin/cut
/usr/bin/file
/usr/bin/grep
/usr/bin/head
/usr/bin/sort
/usr/bin/wc
/usr/bin/xargs
```

</details>
<details>
<summary>Syscall sets</summary>

This contains the syscall sets (as generated by `sysfilter`) for each program executed in the benchmark. Example output:

```
===============================================================================
====[[ Syscall Policy Results ]]=================================================

/usr/bin/bash [84 syscalls]:
Numbers:
    [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 19, 20, 21,
    22, 23, 24, 25, 28, 32, 33, 37, 38, 39, 41, 42, 44, 45, 47, 49, 51,
    52, 54, 56, 59, 60, 61, 62, 63, 72, 79, 80, 82, 83, 87, 89, 91, 92,
    95, 96, 98, 99, 102, 104, 107, 108, 109, 110, 111, 115, 117, 119,
    131, 137, 186, 201, 202, 217, 228, 230, 231, 234, 257, 262, 269,
    270, 302, 307, 318]
Names:
    [0:read, 1:write, 3:close, 4:stat, 5:fstat, 6:lstat, 7:poll,
    8:lseek, 9:mmap, 10:mprotect, 11:munmap, 12:brk, 13:rt_sigaction,
    14:rt_sigprocmask, 15:rt_sigreturn, 16:ioctl, 19:readv, 20:writev,
    21:access, 22:pipe, 23:select, 24:sched_yield, 25:mremap,
    28:madvise, 32:dup, 33:dup2, 37:alarm, 38:setitimer, 39:getpid,
    41:socket, 42:connect, 44:sendto, 45:recvfrom, 47:recvmsg,
    49:bind, 51:getsockname, 52:getpeername, 54:setsockopt, 56:clone,
    59:execve, 60:exit, 61:wait4, 62:kill, 63:uname, 72:fcntl,
    79:getcwd, 80:chdir, 82:rename, 83:mkdir, 87:unlink, 89:readlink,
    91:fchmod, 92:chown, 95:umask, 96:gettimeofday, 98:getrusage,
    99:sysinfo, 102:getuid, 104:getgid, 107:geteuid, 108:getegid,
    109:setpgid, 110:getppid, 111:getpgrp, 115:getgroups, 117:setresuid,
    119:setresgid, 131:sigaltstack, 137:statfs, 186:gettid, 201:time,
    202:futex, 217:getdents64, 228:clock_gettime, 230:clock_nanosleep,
    231:exit_group, 234:tgkill, 257:openat, 262:newfstatat, 269:faccessat,
    270:pselect6, 302:prlimit64, 307:sendmmsg, 318:getrandom]

/usr/bin/cat [47 syscalls]:
Numbers:
    [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 24, 25,
    28, 32, 39, 41, 42, 44, 45, 47, 49, 51, 54, 60, 62, 72, 79, 96, 99,
    186, 201, 202, 217, 221, 228, 231, 234, 257, 262, 302]
Names:
    [0:read, 1:write, 3:close, 4:stat, 5:fstat, 6:lstat, 7:poll,
    8:lseek, 9:mmap, 10:mprotect, 11:munmap, 12:brk, 13:rt_sigaction,
    14:rt_sigprocmask, 15:rt_sigreturn, 16:ioctl, 20:writev,
    24:sched_yield, 25:mremap, 28:madvise, 32:dup, 39:getpid,
    41:socket, 42:connect, 44:sendto, 45:recvfrom, 47:recvmsg, 49:bind,
    51:getsockname, 54:setsockopt, 60:exit, 62:kill, 72:fcntl, 79:getcwd,
    96:gettimeofday, 99:sysinfo, 186:gettid, 201:time, 202:futex,
    217:getdents64, 221:fadvise64, 228:clock_gettime, 231:exit_group,
    234:tgkill, 257:openat, 262:newfstatat, 302:prlimit64]

...
```

</details>
<details>
<summary>Descendant program analysis</summary>

This contains a set of tables describing the changes in syscall sets when one program executes another starting with programs at a depth of 2 (one `execve` off of the first program run). For example, in the output below:

```
===============================================================================
====[[ Syscall Set Analysis: Descendant Contributions ]]=======================

/usr/bin/xargs > (/usr/bin/file) [depth: 2]
Descendant  Root  Shared  Name
---------------------------------------------------------------------------
--          --    0       read
--          --    1       write
--          --    3       close
--          --    4       stat
--          --    5       fstat
--          --    6       lstat
--          --    7       poll
--          --    8       lseek
--          --    9       mmap
--          --    10      mprotect
--          --    11      munmap
--          --    12      brk
--          --    13      rt_sigaction
--          --    14      rt_sigprocmask
--          --    15      rt_sigreturn
--          --    16      ioctl
17          --    --      pread64
--          --    20      writev
21          --    --      access
--          --    22      pipe
23          --    --      select
--          --    24      sched_yield
--          --    25      mremap
--          --    28      madvise
--          --    32      dup
--          --    33      dup2
--          --    39      getpid
--          --    41      socket
--          --    42      connect
--          --    44      sendto
--          --    45      recvfrom
--          --    47      recvmsg
--          --    49      bind
--          --    51      getsockname
--          --    54      setsockopt
--          --    56      clone
58          --    --      vfork
--          --    59      execve
--          --    60      exit
--          --    61      wait4
--          --    62      kill
--          --    72      fcntl
--          --    79      getcwd
83          --    --      mkdir
87          --    --      unlink
89          --    --      readlink
95          --    --      umask
--          --    96      gettimeofday
--          --    99      sysinfo
--          --    186     gettid
--          --    201     time
--          --    202     futex
--          --    217     getdents64
--          --    228     clock_gettime
--          --    231     exit_group
--          --    234     tgkill
235         --    --      utimes
--          --    257     openat
--          --    262     newfstatat
--          --    302     prlimit64
---------------------------------------------------------------------------
9           0     51      TOTAL           [Sum:  60  (+17.6471%)]
---------------------------------------------------------------------------

Root [0 syscalls]
Numbers:
    []
Names:
    []

Descendant [9 syscalls]
Numbers:
    [17, 21, 23, 58, 83, 87, 89, 95, 235]
Names:
    [17:pread64, 21:access, 23:select, 58:vfork, 83:mkdir, 87:unlink,
    89:readlink, 95:umask, 235:utimes]

Shared [51 syscalls]
Numbers:
    [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 22, 24,
    25, 28, 32, 33, 39, 41, 42, 44, 45, 47, 49, 51, 54, 56, 59, 60, 61,
    62, 72, 79, 96, 99, 186, 201, 202, 217, 228, 231, 234, 257, 262, 302]
Names:
    [0:read, 1:write, 3:close, 4:stat, 5:fstat, 6:lstat, 7:poll,
    8:lseek, 9:mmap, 10:mprotect, 11:munmap, 12:brk, 13:rt_sigaction,
    14:rt_sigprocmask, 15:rt_sigreturn, 16:ioctl, 20:writev, 22:pipe,
    24:sched_yield, 25:mremap, 28:madvise, 32:dup, 33:dup2, 39:getpid,
    41:socket, 42:connect, 44:sendto, 45:recvfrom, 47:recvmsg, 49:bind,
    51:getsockname, 54:setsockopt, 56:clone, 59:execve, 60:exit,
    61:wait4, 62:kill, 72:fcntl, 79:getcwd, 96:gettimeofday, 99:sysinfo,
    186:gettid, 201:time, 202:futex, 217:getdents64, 228:clock_gettime,
    231:exit_group, 234:tgkill, 257:openat, 262:newfstatat, 302:prlimit64]

...
```

`/usr/bin/xargs` executes (`>`) `/usr/bin/file` which happens at a depth of 2 from the root program (the first program run, `bash`, has a depth of 1). The first column of the table shows the syscalls that are in descendant program (`/usr/bin/file`) and not in the initial program (`/usr/bin/xargs`). The second column shows the syscalls that are in the initial program (`/usr/bin/xargs`) which are not in the descendant program (`/usr/bin/file`). The third column shows syscalls that are shared by the two programs. The last row of the table summarizes the results. As can be seen in the example above, `/usr/bin/file` requires 9 additional syscalls which `/usr/bin/xargs` does not. Thus, if executed under the inheritance model, `/usr/bin/xargs` would experience 17.65% over-privilege (since it must have these 9 uneccessary syscalls in its allowed set).
</details>
<details>
<summary>Root program analysis</summary>

This contains a table that presents an analysis of the root program's (program that is executed first; for example, `bash`) syscall set compared with **all** descendant programs. A more complete description can be found above in the descendant program analysis section. Example output:

```
===============================================================================
====[[ Syscall Set Analysis: Root Comparison ]]================================

/usr/bin/bash > (all programs)
Descendant  Root  Shared  Name
---------------------------------------------------------------------------
--          --    0       read
--          --    1       write
--          --    3       close
--          --    4       stat
--          --    5       fstat
--          --    6       lstat
--          --    7       poll
--          --    8       lseek
--          --    9       mmap
--          --    10      mprotect
--          --    11      munmap
--          --    12      brk
--          --    13      rt_sigaction
--          --    14      rt_sigprocmask
--          --    15      rt_sigreturn
--          --    16      ioctl
17          --    --      pread64
--          19    --      readv
--          --    20      writev
--          --    21      access
--          --    22      pipe
--          --    23      select
--          --    24      sched_yield
--          --    25      mremap
--          --    28      madvise
--          --    32      dup
--          --    33      dup2
34          --    --      pause
--          37    --      alarm
--          38    --      setitimer
--          --    39      getpid
--          --    41      socket
--          --    42      connect
--          --    44      sendto
--          --    45      recvfrom
--          --    47      recvmsg
--          --    49      bind
--          --    51      getsockname
--          52    --      getpeername
--          --    54      setsockopt
--          --    56      clone
58          --    --      vfork
--          --    59      execve
--          --    60      exit
--          --    61      wait4
--          --    62      kill
--          63    --      uname
--          --    72      fcntl
77          --    --      ftruncate
--          --    79      getcwd
--          80    --      chdir
81          --    --      fchdir
--          82    --      rename
--          --    83      mkdir
--          --    87      unlink
--          --    89      readlink
--          91    --      fchmod
--          92    --      chown
--          --    95      umask
--          --    96      gettimeofday
--          98    --      getrusage
--          --    99      sysinfo
--          --    102     getuid
--          --    104     getgid
105         --    --      setuid
106         --    --      setgid
--          --    107     geteuid
--          --    108     getegid
--          109   --      setpgid
--          --    110     getppid
--          111   --      getpgrp
113         --    --      setreuid
114         --    --      setregid
--          --    115     getgroups
116         --    --      setgroups
--          --    117     setresuid
--          --    119     setresgid
--          --    131     sigaltstack
--          137   --      statfs
138         --    --      fstatfs
143         --    --      sched_getparam
144         --    --      sched_setscheduler
145         --    --      sched_getscheduler
146         --    --      sched_get_priority_max
147         --    --      sched_get_priority_min
--          --    186     gettid
--          --    201     time
--          --    202     futex
203         --    --      sched_setaffinity
204         --    --      sched_getaffinity
--          --    217     getdents64
218         --    --      set_tid_address
221         --    --      fadvise64
--          --    228     clock_gettime
--          --    230     clock_nanosleep
--          --    231     exit_group
--          --    234     tgkill
235         --    --      utimes
--          --    257     openat
--          --    262     newfstatat
--          269   --      faccessat
--          270   --      pselect6
273         --    --      set_robust_list
275         --    --      splice
293         --    --      pipe2
--          --    302     prlimit64
--          307   --      sendmmsg
--          318   --      getrandom
---------------------------------------------------------------------------
24          17    67      TOTAL                   [Sum:  108  (+28.5714%)]
---------------------------------------------------------------------------

Root [17 syscalls]
Numbers:
    [19, 37, 38, 52, 63, 80, 82, 91, 92, 98, 109, 111, 137, 269, 270,
    307, 318]
Names:
    [19:readv, 37:alarm, 38:setitimer, 52:getpeername, 63:uname,
    80:chdir, 82:rename, 91:fchmod, 92:chown, 98:getrusage, 109:setpgid,
    111:getpgrp, 137:statfs, 269:faccessat, 270:pselect6, 307:sendmmsg,
    318:getrandom]

Descendant [24 syscalls]
Numbers:
    [17, 34, 58, 77, 81, 105, 106, 113, 114, 116, 138, 143, 144, 145,
    146, 147, 203, 204, 218, 221, 235, 273, 275, 293]
Names:
    [17:pread64, 34:pause, 58:vfork, 77:ftruncate, 81:fchdir, 105:setuid,
    106:setgid, 113:setreuid, 114:setregid, 116:setgroups, 138:fstatfs,
    143:sched_getparam, 144:sched_setscheduler, 145:sched_getscheduler,
    146:sched_get_priority_max, 147:sched_get_priority_min,
    203:sched_setaffinity, 204:sched_getaffinity, 218:set_tid_address,
    221:fadvise64, 235:utimes, 273:set_robust_list, 275:splice, 293:pipe2]

Shared [67 syscalls]
Numbers:
    [0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 20, 21, 22,
    23, 24, 25, 28, 32, 33, 39, 41, 42, 44, 45, 47, 49, 51, 54, 56, 59,
    60, 61, 62, 72, 79, 83, 87, 89, 95, 96, 99, 102, 104, 107, 108,
    110, 115, 117, 119, 131, 186, 201, 202, 217, 228, 230, 231, 234,
    257, 262, 302]
Names:
    [0:read, 1:write, 3:close, 4:stat, 5:fstat, 6:lstat, 7:poll,
    8:lseek, 9:mmap, 10:mprotect, 11:munmap, 12:brk, 13:rt_sigaction,
    14:rt_sigprocmask, 15:rt_sigreturn, 16:ioctl, 20:writev, 21:access,
    22:pipe, 23:select, 24:sched_yield, 25:mremap, 28:madvise, 32:dup,
    33:dup2, 39:getpid, 41:socket, 42:connect, 44:sendto, 45:recvfrom,
    47:recvmsg, 49:bind, 51:getsockname, 54:setsockopt, 56:clone,
    59:execve, 60:exit, 61:wait4, 62:kill, 72:fcntl, 79:getcwd, 83:mkdir,
    87:unlink, 89:readlink, 95:umask, 96:gettimeofday, 99:sysinfo,
    102:getuid, 104:getgid, 107:geteuid, 108:getegid, 110:getppid,
    115:getgroups, 117:setresuid, 119:setresgid, 131:sigaltstack,
    186:gettid, 201:time, 202:futex, 217:getdents64, 228:clock_gettime,
    230:clock_nanosleep, 231:exit_group, 234:tgkill, 257:openat,
    262:newfstatat, 302:prlimit64]
```

</details>
<details>
<summary>Table summaries</summary>

These sections contain table summaries of the previous sections. These results can be used to compare with Table 4 in the paper. The first section is a textual summary and the second section presents LaTeX output.

```
===============================================================================
====[[ Syscall Set Analysis: Table Summary ]]==================================

Depth  Root   Descendants                             TotalSyscalls  PercentInc
1      bash   [cat,cut,file,grep,head,sort,wc,xargs]  108            (+28.57%)
2      xargs  [file]                                  60             (+17.65%)
2      xargs  [wc]                                    52             (+1.96%)

===============================================================================
====[[ Syscall Set Analysis: Table Summary (LaTeX) ]]==========================

1	&	\texttt{bash}	&	\texttt{cat}, \texttt{cut}, \texttt{file}, \texttt{grep}, \texttt{head}, \texttt{sort}, \texttt{wc}, \texttt{xargs}	&	108	&	(+28.57\%) \\

2	&	\texttt{xargs}	&	\texttt{file}	&	60	&	(+17.65\%) \\
2	&	\texttt{xargs}	&	\texttt{wc}	&	52	&	(+1.96\%) \\
```

</details>

To determine reproducibility, please compare the table summary of each results file with the corresponding rows in Table 4 of the paper. Alternatively, we have included example output that compared against in [`misc/effectiveness-results.tar.gz`](./misc/effectiveness-results.tar.gz).

## Debugging

To drop into a shell in the VM, the following command can be run from root of this repository on the host machine:

```shell
./tools/sysxchg debug <vanilla | inheritance | exchange>
```

(The username is `root` and there is no password.)

Note that if you also built debug kernels (not done by default) with:

```shell
./tools/sysxchg build kernel <vanilla-dbg | inheritance-dbg | exchange-dbg>
```

you can also run:

```shell
./tools/sysxchg debug <vanilla-dbg | inheritance-dbg | exchange-dbg>
```

## Cleaning

To clean the `build` directory, run:

```shell
./tools/sysxchg clean all
```

<span style="color:orange">NOTE: this should be run from the root of this repository **on the host machine**.</span>

`sysxchg clean` can also clean individual directories in `build`. To see its usage, run:

```shell
./tools/sysxchg clean help
```

## License

This software uses the [BSD License](./LICENSE).

