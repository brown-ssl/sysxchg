_sysxchg() {
    local cur prev opts
    COMPREPLY=()
    cur="${COMP_WORDS[COMP_CWORD]}"
    prev="${COMP_WORDS[COMP_CWORD-1]}"
    
    case "${prev}" in
        build|clean)
            opts="all kernel rootfs users tests benchmark"
            ;;
        kernel)
            opts="vanilla vanilla-dbg inheritance inheritance-dbg exchange exchange-dbg"
            ;;
        benchmark)
            opts="spec sqlite pash"
            ;;
        run)
            opts="tests performance effectiveness"
            ;;
        tests)
            opts="inheritance exchange"
            ;;
        performance)
            opts="spec sqlite pash"
            ;;
        debug)
            opts="vanilla inheritance exchange"
            ;;
        *)
            opts="build clean run debug"
            ;;
    esac
    
    COMPREPLY=( $(compgen -W "${opts}" -- ${cur}) )
    return 0
}
complete -F _sysxchg sysxchg
